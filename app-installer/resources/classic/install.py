#!/usr/bin/python
#@author Shourien

import optparse
import os
import shutil
import sys
import zipfile
from xml.dom import minidom
from ConfigParser import SafeConfigParser, NoSectionError, NoOptionError
from os import getcwd, chmod, makedirs
from os.path import join, isfile, exists

def read_app_cfg(conf_file):
    if not isfile(conf_file):
        raise InstallationException("No File with name " + conf_file + " in " + getcwd())
    conf = SafeConfigParser()
    conf.optionxform = str
    conf.read(conf_file)
    return conf

def get_install_dir(conf):
    install_dir = conf.get('Installation', 'app.install.dir')
    print_line("Installing at %s" % install_dir)
    return install_dir

def fetch_wildfly_dir(jboss_dir):
    wildfly = [x for x in os.listdir(jboss_dir) if x.startswith('wildfly')]
    return wildfly[0]

def find_element(root, name):
    return root.getElementsByTagName(name)[0]

def replace_element(root, name, value):
    element = find_element(root, name)
    element.firstChild.replaceWholeText(value)

def jboss_install(jboss_dir):
    try:
        if os.path.exists(jboss_dir):
            print_operation("Deleting old installation %s" % jboss_dir)
            shutil.rmtree(jboss_dir)
            print_done()
        print_operation("Creating new installation directory %s" % jboss_dir)
        os.makedirs(jboss_dir)
        print_done()
        print_operation("Installing 'JBoss' to %s" % jboss_dir)
        files = [f for f in os.listdir('.') if f.endswith('.zip')]
        for file in files:
            zip_ref = zipfile.ZipFile(file, 'r')
            zip_ref.extractall(jboss_dir)
            zip_ref.close()
        chmod(join(jboss_dir, fetch_wildfly_dir(jboss_dir), 'bin', 'standalone.sh'), 0774)
        chmod(join(jboss_dir, fetch_wildfly_dir(jboss_dir), 'bin', 'app.py'), 0774)
        print_done()
    except IOError, (errno, strerror):
        print "I/O error(%s): %s" % (errno, strerror),
        raise InstallationException("No Directory with name jboss")

def write_app_conf(jboss_dir, conf):
    bin_dir = join(jboss_dir, fetch_wildfly_dir(jboss_dir), 'bin')
    print_operation("Generating app.conf at %s" % bin_dir)

    appconf = SafeConfigParser()
    appconf.optionxform = str

    try:
        appconf.add_section('Deploy_Mode')
        appconf.set('Deploy_Mode', 'java.home', conf.get('Installation', 'java.jdk.home'))

        appconf.add_section('JBoss_Java_Options')

        for key, value in conf.items('JBoss_Java_Options'): 
            if key not in [ 'jboss.socket.binding.port-offset', 'jboss.bind.address', 'jboss.management.http.port']:
                raise InstallationException('Unsupported key in [JBoss_Java_Options]: ' + key)

            appconf.set('JBoss_Java_Options', key, value)

        for section in ['JBoss_Java_Options']:
            if conf.has_section(section):
                if not appconf.has_section(section):
                    appconf.add_section(section)
                for key, value in conf.items(section):
                    appconf.set(section, key, value)

        appconf.add_section('DB_Configuration')

        for key, value in conf.items('DB_Configuration'): 
            if key not in [ 'app.jdbc.connection.url', 'app.connection.username', 'app.connection.password']:
                raise InstallationException('Unsupported key in [DB_Configuration]: ' + key)

            appconf.set('DB_Configuration', key, value)

        for section in ['DB_Configuration']:
            if conf.has_section(section):
                if not appconf.has_section(section):
                    appconf.add_section(section)
                for key, value in conf.items(section):
                    appconf.set(section, key, value)

    except IOError, (errno, strerror):
        print "I/O error(%s): %s" % (errno, strerror),
        raise InstallationException("Error while writing to app.conf")
    else:
        with open(join(bin_dir, 'app.conf'), 'w') as conffile:
            appconf.write(conffile)
        print_done()


def print_boxed(*args):
    lineLength=80
    print "\n#################################################################################"
    for line in args:
       start = lineLength/2 - len(line)/2
       sys.stdout.write("#")
       for i in range(1, start):
          sys.stdout.write(" ")
       sys.stdout.write(line)
       for i in range(start + len(line), lineLength):
          sys.stdout.write(" ")
       print("#")
    print "#################################################################################\n"
    sys.stdout.flush()


def print_line(message):
    print "\n         "  + message

def print_operation(operation):
    print "\n         "  + operation + " ......",

def print_done():
    print "done"

def getArguments():
    parser = optparse.OptionParser()
    installOptions = optparse.OptionGroup(parser, 'Application install options')
    installOptions.add_option('--conf-file', type="string", help='Installation configuration file name. Default would be app.cfg')  
    
    parser.add_option_group(installOptions)
    (options,args) = parser.parse_args()
    count=0
    for k,v in options.__dict__.items():
        if v:
           count = count + 1
    if count > 1:
        parser.error("Can specify only --conf-file as additional option, Need to have only one or no arguments")               
    return options

class InstallationException(Exception):
    def __init__(self, value):
        self.parameter = value

    def __str__(self):
        return repr(self.parameter)

def main():
    print 'APPLICATION installer. Use --help for options'
    print "\n"
    cli_options = getArguments()
    conf_file = 'app.cfg'
    if cli_options.conf_file:
        conf_file = cli_options.conf_file
    conf = read_app_cfg(conf_file)

    jboss_dir = get_install_dir(conf)
    jboss_install(jboss_dir)
    write_app_conf(jboss_dir, conf)
    os.chdir(jboss_dir)

def fail_installation(error_message):
    print_line(error_message)
    print_boxed('Installation Failed')
    sys.exit(1)

if __name__ == "__main__":
    try:
        main()
    except NoSectionError, msg:
        fail_installation("Missing section [" + msg.section + "] in app.cfg")
    except NoOptionError, msg:
        fail_installation("Missing option '" + msg.option + "' of section [" + msg.section + "] in app.cfg")
    except InstallationException, e:
        fail_installation(e.parameter)
    else:
        print_boxed('Installation Completed Successfully')
