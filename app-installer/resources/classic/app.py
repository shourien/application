#!/usr/bin/python
# @author Shourien

import os
import re
import socket
import signal
import subprocess
import sys
import time
from ConfigParser import SafeConfigParser, NoSectionError
from fileinput import FileInput
from os import path, putenv, getenv, mkdir, makedirs
from os.path import join, exists
from platform import system
from shutil import copy

socket.setdefaulttimeout(5)
sys.stdout = os.fdopen(sys.stdout.fileno(), 'w', 0)

class JbossState:
    def __init__(self, label, startable, stoppable):
        self.label = label
        self.startable = startable
        self.stoppable = stoppable

    def __repr__(self):
        return self.label


STOPPED = JbossState('stopped', True, False)
STARTING = JbossState('starting', False, True)
STARTED = JbossState('started', False, True)
FAILED = JbossState('failed', False, True)


class OSFamily:
    def __init__(self, label):
        self.label = label

    def __repr__(self):
        return self.label
WINDOWS = OSFamily('Windows')
UNIX = OSFamily('Unix')


def getIpAddress(address):
    idx = -1
    try:
        idx = address.index('[')
    except ValueError:
        idx = -1
    if idx > -1:
        return address[1:-1]
    else:
        return address


def _get_os_family():
    platform = system()
    if platform == 'Windows':
        return WINDOWS
    elif platform == 'Linux' or platform == 'SunOS':
        return UNIX
    else:
        raise 'Unsupported Platform: ' + platform


class JbossCtl:
    def __init__(self, conf):
        self.conf = conf
        set_java_env(conf)
        self.jboss_bind_address = _get_jboss_bind_address(conf)
        self.binding_set = "jboss.socket.binding.port-offset=" + conf.get('JBoss_Java_Options', 'jboss.socket.binding.port-offset')
        self.deploy_dir = path.join(_get_standalone_dir(conf), 'deployments')

    def executeCommand(self, cmd_line):
        bin_dir = get_bin_dir()
        os_family = _get_os_family()
        java_opts = getenv('JAVA_OPTS', '')

        putenv('JAVA_OPTS', java_opts)

        if os_family == WINDOWS:
            proc = subprocess.Popen(cmd_line, shell=False, stdout=subprocess.PIPE, stdin=subprocess.PIPE)
        elif os_family == UNIX:
            proc = subprocess.Popen(cmd_line, shell=True, cwd=bin_dir, stdout=subprocess.PIPE)

        output, err = proc.communicate()

        return output


    def _checkDeploymentStatus(self):
        try:
            os_family = _get_os_family()
            if os_family == WINDOWS:
                cmd_line = get_jboss_cli_cmd(conf, ' --connect ' + get_jnp_arguments(
                    conf) + ' --command="/deployment=*:read-attribute(name=status)"')
            else:
                cmd_line = get_jboss_cli_cmd(conf, ' --connect ' + get_jnp_arguments(
                    conf) + ' --command="/deployment=*:read-attribute(name=status)" 2>/dev/null')
            output = self.executeCommand(cmd_line)

            if output == '' or output.find('System boot is in process') != -1:
                return STARTING
            elif output.find('Exception') != -1 or output.find('JDWP') != -1 or output.find('"outcome" => "success"') == -1:
                return STARTING
            else:
                if output.count('"result"') > 0:
                    if output.count('"result" => "OK"') == output.count('"result"') - 1:
                        return STARTED
                    else:
                        cmd_line = get_jboss_cli_cmd(conf, ' --connect ' + get_jnp_arguments(conf) + ' --command="/deployment=*/subdeployment=*/subsystem=web/servlet=*:read-attribute(name=requestCount)" 2>/dev/null')
                        output2 = self.executeCommand(cmd_line)
                        if output2.count('"result"') > 0:
                            if output2.count('"outcome" => "success"') == output2.count('"result"'):
                                return STARTED
                            else:
                                print output
                                return FAILED
                        else:
                            print output
                            return FAILED

            print "Can not connect to JBoss CLI ..."
            return FAILED

        except Exception, e:
            print e
            return FAILED


    def _findPids(self):
        os_family = _get_os_family()
        if os_family == WINDOWS:
            return self._findPidsInWindows()
        elif os_family == UNIX:
            return self._findPidsInUnix()


    def print_pids(self):
        pids = self._findPids()
        return ','.join(pids)


    def _findPidsInUnix(self):
        ps = subprocess.Popen(
            "ps -eafww | grep java |  grep " + self.jboss_bind_address + " | grep " + self.binding_set + " | grep -v grep",
            shell=True, stdout=subprocess.PIPE)
        output = ps.stdout.read()
        ps.stdout.close()
        ps.wait()
        output = output.split("\n")
        ids = []
        for process in output:
            process = process.strip()
            if len(process) > 0:
                ids.append(process.split()[1])
        return ids


    def _findPidsInWindows(self):
        java_home = self.conf.get('Deploy_Mode', 'java.home', False, os.environ)
        ps = subprocess.Popen("\"" + path.join(java_home, "bin", "jps") + "\" -mv", shell=True, stdout=subprocess.PIPE)
        output = ps.stdout.read()
        ps.stdout.close()
        ps.wait()
        output = output.split("\n")
        ids = []
        for process in output:
            process = process.strip()
            if len(process) > 0:
                if self.binding_set in process:
                    ids.append(process.split()[0])
        return ids


    def get_state(self):
        pids = self._findPids()
        if pids:
            return self._checkDeploymentStatus()
        else:
            return STOPPED


    def kill(self):
        pids = self._findPids()
        for pid in pids:
            os_family = _get_os_family()
            if os_family == WINDOWS:
                subprocess.Popen("taskkill /F /PID " + pid, shell=True, stdout=subprocess.PIPE)
            elif os_family == UNIX:
                subprocess.Popen("kill -9 %s" % pid, shell=True, stdout=subprocess.PIPE)


    def wait_for_kill(self):
        print 'Waiting for killing the server . .',
        startTime = time.time()
        state = self.get_state()
        while state != STOPPED and startTime + 100 > time.time():
            print '.',
            time.sleep(1)
            if state == STOPPED:
                print 'Server successfully shutdown'


    def wait_for_stop(self):
        print 'Waiting for server to shutdown . .',
        startTime = time.time()
        state = self.get_state()
        while state != STOPPED and startTime + 100 > time.time():
            print '.',
            time.sleep(2)
            state = self.get_state()
        if state != STOPPED:
            print 'The server did not react to the shutdown command. Killing it..'
            self.kill()
        else:
            print 'Server successfully shutdown'


    def wait_for_start(self):
        print 'Waiting for server to start . .',
        startTime = time.time()
        time.sleep(5)
        state = self.get_state()
        while state == STARTING and startTime + 600 > time.time():
            print '.',
            time.sleep(2)
            state = self.get_state()
        if state == STARTED:
            print '\nServer successfully started in %i seconds' % (time.time() - startTime)
            return 0
        elif state == STARTING:
            print 'Server start timed out. Check logs for details'
        else:
            print 'Server failed to start. Check logs for details'
        return 1


def get_bin_dir():
    return path.dirname(path.realpath(__file__))


def read_app_conf(conf_file):
    bin_dir = get_bin_dir()
    if path.isfile(conf_file):
        f_name = conf_file
    else:
        f_name = path.join(bin_dir, 'app.conf')
    if not path.isfile(f_name):
        raise IOError("Config file not found: " + f_name)
    conf = SafeConfigParser()
    conf.optionxform = str
    conf.read(f_name)
    return conf

def _get_jboss_bind_address(conf):
    return getIpAddress(conf.get('JBoss_Java_Options', 'jboss.bind.address'))

def _get_standalone_dir(conf):
    return path.join(path.dirname(get_bin_dir()), 'standalone')

def get_jnp_arguments(conf):
    management_port = str(int(conf.get('JBoss_Java_Options', 'jboss.management.http.port')) + int(conf.get('JBoss_Java_Options', 'jboss.socket.binding.port-offset')))
    return ' --controller=' + get_optional_value(conf, 'JBoss_Java_Options', 'jboss.bind.address.management', '127.0.0.1') + ':' + management_port


def stop_jboss(conf):
    jbossChecker = JbossCtl(conf)
    state = jbossChecker.get_state()
    pids = jbossChecker.print_pids()
    if not state.stoppable:
        print 'Server not running'
        return 0
    if state.stoppable:
        print 'Running process ..', pids
    set_java_env(conf)
    arguments = ' --connect command=:shutdown ' + get_jnp_arguments(conf)
    bin_dir = get_bin_dir()
    os_family = _get_os_family()
    if os_family == WINDOWS:
        cmd_line = get_jboss_cli_cmd(conf, arguments)
        result = subprocess.call(cmd_line, shell=False)
    elif os_family == UNIX:
        cmd_line = get_jboss_cli_cmd(conf, arguments + ' 2>&1 >/dev/null')
        result = subprocess.call(cmd_line, shell=True, cwd=bin_dir)
    if result:
        print 'Failed to launch shutdown command. Killing server'
        jbossChecker.kill()
        jbossChecker.wait_for_kill()
    else:
        jbossChecker.wait_for_stop()
    if not jbossChecker.print_pids():
        print '\nNo processes is running'
    else:
        print_pids = jbossChecker.print_pids()
        print '\nStill these processes are running.', print_pids


def _trim_quotes(str):
    return str.lstrip('\'" ').rstrip('\'" ')


def _construct_java_opts(conf):
    val = dict([k, _trim_quotes(v)] for k, v in conf.items('JBoss_Java_Options'))
    java_opts = getenv('JAVA_OPTS', '')
    for k, v in val.items():
        java_opts += ' -D' + k + '=' + v

    for section in ['DB_Configuration']:
        for k, v in conf.items(section, True):
            iv = conf.get(section, k, False, os.environ)
            java_opts += ' -D' + k + '=' + iv
    return java_opts


def set_java_env(conf):
    java_home = conf.get('Deploy_Mode', 'java.home', False, os.environ)
    putenv('JAVA_HOME', java_home)


def get_jboss_cli_cmd(conf, arguments):
    bin_dir = get_bin_dir()
    os_family = _get_os_family()
    if os_family == WINDOWS:
        putenv('NOPAUSE', 'true')
        executable = path.join(bin_dir, 'jboss-cli.bat')
        cmd_line = executable + ' ' + arguments
    elif os_family == UNIX:
        executable = path.join(bin_dir, 'jboss-cli.sh')
        cmd_line = executable + ' ' + arguments
    return cmd_line

def get_optional_value(conf, option_name, key_name, default_value):
    if conf.has_option(option_name, key_name):
        return conf.get(option_name, key_name)
    else:
        return default_value


def start_jboss(conf):
    jbossChecker = JbossCtl(conf)
    state = jbossChecker.get_state()
    if not state.startable:
        print 'Can not start jboss in state ' + str(state)
        return 0

    bin_dir = get_bin_dir()
    java_opts = _construct_java_opts(conf)
    
    putenv('JAVA_OPTS', java_opts)
    if not os.environ.get('run_foreground') != None:
        putenv('LAUNCH_JBOSS_IN_BACKGROUND', "1")
    pid_dir = ''
    if pid_dir == '':
        pid_dir = path.join(_get_standalone_dir(conf), 'tmp')
    putenv('JBOSS_PIDFILE', path.join(pid_dir, 'jboss-as-standalone.pid'))
    set_java_env(conf)
    os_family = _get_os_family()

    log_dir = ''
    if log_dir == '':
        log_dir = path.join(_get_standalone_dir(conf), 'log')

    consoleout_file = path.join(log_dir, 'console.out')
    if not exists(log_dir):
        mkdir(log_dir)

    consoleout_filehandle = open(consoleout_file, 'w')

    if os_family == WINDOWS:
        cmd_line = path.join(bin_dir, 'standalone.bat')
        subprocess.Popen(cmd_line, stdout=consoleout_filehandle, stderr=subprocess.STDOUT, cwd=bin_dir)
        return jbossChecker.wait_for_start()
    elif os_family == UNIX:
        cmd_line = path.join(bin_dir, 'standalone.sh ')
        if not os.environ.get('run_foreground') != None:
            cmd_line = 'nohup ' + cmd_line + ' > ' + consoleout_file + ' 2>&1 &'
            result = subprocess.call(cmd_line, shell=True, cwd=bin_dir)
            if result:
                print 'Failed to launch start script. Returned error code: ' + str(result)
                return result

            return jbossChecker.wait_for_start()
        else:
            def stop(signum, frame):
                stop_jboss(conf)
            signal.signal(signal.SIGTERM, stop)
            return subprocess.call(cmd_line, shell=True, cwd=bin_dir)


def is_jboss_running(conf):
    jbossChecker = JbossCtl(conf)
    state = jbossChecker.get_state()
    return state == STARTED

def usage():
    print "Usage: app.py "
    print "start : Starts APP Server "
    print "stop : Stops APP Server"
    print "restart : Restarts APP Server"
    print "status : Displays current status of server (started/stopped)"
    sys.exit()


def check_python_version():
    info = sys.version_info
    version = '%s.%s' % (info[0], info[1])
    if float(version) < 2.4:
        raise Exception('Python %s is not supported. Should have at least 2.4' % version)


if __name__ == "__main__":
    argv = sys.argv
    if len(argv) == 1:
        # stop the program and print an error message
        usage()

    check_python_version()
    if len(argv) > 2:
        conf = read_app_conf(argv[2])
    else:
        conf = read_app_conf('')
    exit_code = 0
    cmd = argv[1]
    if cmd == 'start':
        exit_code = start_jboss(conf)
    elif cmd == 'stop':
        exit_code = stop_jboss(conf)
    elif cmd == 'restart':
        stop_jboss(conf)
        exit_code = start_jboss(conf)
    elif cmd == 'status':
        state = JbossCtl(conf).get_state()
        print state
        if state != STARTED:
            exit_code = 1
    else:
        print 'Unrecognized option:', cmd
        usage()
    sys.exit(exit_code)
