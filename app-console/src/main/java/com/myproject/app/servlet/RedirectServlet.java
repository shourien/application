package com.myproject.app.servlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;
import java.util.Optional;

public class RedirectServlet extends HttpServlet {

	/**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = -4297304427712684170L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        HttpSession session = req.getSession(false);
        String targetPath = getServletConfig().getInitParameter("target");
        if (session != null) {
            session.invalidate();
        }
        Optional.of(targetPath).ifPresent(target -> {
            try {
                if (target.startsWith("http")) {
                    res.sendRedirect(target);
                } else {
                    res.sendRedirect(getServletContext().getContextPath() + target);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}
