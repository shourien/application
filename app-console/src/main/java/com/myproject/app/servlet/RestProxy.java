package com.myproject.app.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RestProxy extends HttpServlet {

    /**
	 * Generated Serial Version
	 */
	private static final long serialVersionUID = -5906602760447338196L;

	@Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext context = getServletContext().getContext("/rest");
        RequestDispatcher requestDispatcher = context.getRequestDispatcher(req.getPathInfo());
        if (requestDispatcher != null) {
            try {
                requestDispatcher.forward(req, resp);
            } catch (ServletException | IOException | RuntimeException e) {
                throw e;
            }
        } else {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        }
    }
}
