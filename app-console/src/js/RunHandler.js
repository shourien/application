angular
	.module('AppConsole')
	.run(RunHandler);

function RunHandler($rootScope, $location, $http, $templateCache) {
	$http.get('templates/shared/pagination.html', {cache:$templateCache});
	$rootScope.$on('$stateChangeStart', function (event, next, current) {
		if($rootScope.globals && $location.path() !== '/' && next.data) {
			$location.path('/defaultHome');
		}
	});
};