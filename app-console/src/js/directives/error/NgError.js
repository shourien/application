/**
 * ngError Directive
 */

angular
    .module('AppConsole')
    .directive('ngError', ngError);

function ngError($parse) {
    return {
        restrict: 'A',
        compile: function($element, attr) {
            var fn = $parse(attr['ngError']);
            return function(scope, element, attr) {
                element.on('error', function(event) {
                    scope.$apply(function() {
                        fn(scope, {$event:event});
                    });
                });
            };
        }
    };
};