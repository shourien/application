/**
 * Widget Body Directive
 */

angular
    .module('AppConsole')
    .directive('widgetBody', widgetBody);

function widgetBody() {
    var directive = {
        requires: '^widget',
        transclude: true,
        template: '<div collapse="$parent.collapsible" class="widget-body" ng-class="classes"><div class="widget-content" ng-transclude></div></div>',
        restrict: 'E'
    };
    return directive;
};