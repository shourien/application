/**
 * Widget Header Directive
 */

angular
    .module('AppConsole')
    .directive('widgetHeader', widgetTitle);

function widgetTitle() {
    var directive = {
        requires: '^widget',
        transclude: true,
        template: '<div class="widget-header">' +
        '<div class="row"><div class="pull-left top-buffer">' +
        '<i ng-class="$parent.icon"></i> <strong>{{$parent.heading}}</strong></div>' +
        '<div class="pull-right">' +
        '<i ng-class="{\'glyphicon glyphicon-circle-arrow-down btn pad-btn\': !$parent.collapsible, \' glyphicon glyphicon-circle-arrow-right btn pad-btn\': $parent.collapsible}" ng-click="$parent.collapsible = !$parent.collapsible"></i></div>'+
        '<div class="pull-right" ng-transclude></div></div>',
        restrict: 'E'
    };
    return directive;
};