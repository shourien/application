/**
 * Widget Directive
 */

angular
    .module('AppConsole')
    .directive('widget', widget);

function widget() {
    var directive = {
        transclude: true,
        scope: {
            heading: '@',
            icon: '@',
            ngModel: '=',
            collapsible: '='
        },
        template: '<div class="widget" ng-transclude></div>',
        restrict: 'EA'
    };
    return directive;

    function link(scope, element, attrs) {
        /* */
    }
};