/**
 * Widget Footer Directive
 */

angular
    .module('AppConsole')
    .directive('widgetFooter', widgetFooter);

function widgetFooter() {
    var directive = {
        requires: '^widget',
        transclude: true,
        template: '<div class="widget-footer" ng-transclude></div>',
        restrict: 'E'
    };
    return directive;
};