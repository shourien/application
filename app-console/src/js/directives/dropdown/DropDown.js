/**
 * Dropdown Directive
 */

angular
    .module('AppConsole')
    .directive('dropdown', dropdown);

function dropdown() {
    return {
    	restrict: 'E',
    	require: '^ngModel',
    	scope: {
    		ngModel: '=', // selection
    		callback: '&' // callback
    	},
    	link: function(scope, element, attrs) {
    		element.on('click', function(event) {
    			event.preventDefault();
    		});
        
    		scope.default = 10;
    		scope.isButton = 'isButton' in attrs;
  
    		// selection changed handler
    		scope.select = function(item) {
    			scope.ngModel = item;
    			if (scope.callback) {
    				scope.callback({ item: item });
    			}
    		};
    	},
    	templateUrl: 'templates/shared/pagination_size_dropdown.html'
    };
};
