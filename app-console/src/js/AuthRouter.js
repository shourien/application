/**
 * Route configuration for the Authentication module. 
 */
angular
	.module('Authentication')
	.config(AuthRouter);

function AuthRouter($translateProvider, $stateProvider, $urlRouterProvider) {
	$urlRouterProvider.otherwise('/login');
	$stateProvider
    	.state('Login', {
	        url: '/login',
	        controller: 'AuthController as ac',
	        templateUrl: 'templates/auth/login_view.html'
	    });
	
	$translateProvider.useStaticFilesLoader({
          prefix: 'locales/',
          suffix: '.json'
    });	
	$translateProvider.determinePreferredLanguage();  
	$translateProvider.fallbackLanguage('en');
	$translateProvider.useSanitizeValueStrategy('escaped');
};
