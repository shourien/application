/**
 * Route configuration for the Dashboard module.
 */
angular
	.module('AppConsole')
	.config(Router);

function Router($translateProvider, $stateProvider, $urlRouterProvider, RestangularProvider, growlProvider, stConfig, $breadcrumbProvider) {
	
	$urlRouterProvider.otherwise('/dashboard');
    // Application routes
    $stateProvider
        .state('Dashboard', {
            url: '/dashboard',
            controller: 'DashboardController as dc',
            templateUrl: 'templates/dashboard/dashboard.html',
            ncyBreadcrumb: {
                label: 'Home',
                icon: 'glyphicon glyphicon-home'
            }
        })
        .state('AuthError', {
            url: '/defaultHome',
            templateUrl: 'templates/shared/forbidden.html',
            ncyBreadcrumb: {
                label: 'Home',
                icon: 'glyphicon glyphicon-home'
            } 
        })
        .state('Roles', {
            url: '/roles',
            controller: 'RolesController as rc',
            templateUrl: 'templates/roles/roles_view.html',
            ncyBreadcrumb: {
                label: 'Roles',
                parent: 'Dashboard',
                icon: 'glyphicon glyphicon-education'
            }
        })
        .state('RoleAdd', {
            url: '/roleAdd',
            templateUrl: 'templates/roles/role_add.html',
            controller: 'AddRoleController as mr',
            ncyBreadcrumb: {
                label: 'Add Role',
                parent: 'Roles',
                icon: 'glyphicon glyphicon-plus'
            }
        })
        .state('RoleEdit', {
            url: '/roleEdit/:id',
            templateUrl: 'templates/roles/role_add.html',
            controller: 'EditRoleController as mr',
            ncyBreadcrumb: {
                label: 'Role Edit',
                parent: 'Roles',
                icon: 'glyphicon glyphicon-pencil'
            }
        })
        .state('Users', {
            url: '/users',
            controller: 'UsersController as uc',
            templateUrl: 'templates/users/users_view.html',
            ncyBreadcrumb: {
                label: 'Users',
                parent: 'Dashboard',
                icon: 'glyphicon glyphicon-user'
            }
        })
        .state('UserAdd', {
            url: '/userAdd',
            controller: 'AddUserController as mu',
            templateUrl: 'templates/users/user_add.html',
            ncyBreadcrumb: {
                label: 'Add User',
                parent: 'Users',
                icon: 'glyphicon glyphicon-plus'
            }
        })
        .state('UserEdit', {
            url: '/userEdit/:id',
            controller: 'EditUserController as mu',
            templateUrl: 'templates/users/user_add.html',
            ncyBreadcrumb: {
                label: 'User Edit',
                parent: 'Users',
                icon: 'glyphicon glyphicon-pencil'
            }
        });
    RestangularProvider.setBaseUrl('rest-proxy');
    growlProvider.globalReversedOrder(true);
    growlProvider.globalPosition('top-right');
    growlProvider.globalTimeToLive({success: 5000, warning: 5000, info: 5000});
    stConfig.pagination.template = 'templates/shared/pagination.html';
    $breadcrumbProvider.setOptions({
    	templateUrl: 'templates/shared/breadcrumb.html',
    });
    $translateProvider.useStaticFilesLoader({
    	prefix: 'locales/',
    	suffix: '.json'
    });		
	$translateProvider.determinePreferredLanguage();  
	$translateProvider.fallbackLanguage('en');
	$translateProvider.useSanitizeValueStrategy('escaped');
    Highcharts.setOptions({
        global : {
            useUTC : false
        }
    });
};
