/**
 *  Authentication Main Run
 */

angular
	.module('Authentication')
	.run(AuthRunHandler);

function AuthRunHandler($rootScope, $state) {
	$rootScope.$on('$locationChangeStart', function (event, next, current) {
	    // redirect to login page if not logged in
		$state.transitionTo('Login');
	});
};