﻿/**
 *  Authentication Controller
 */

angular.module('Authentication')
	 .controller('AuthController', AuthController);

function AuthController($translate, $window, AuthenticationService) {
	var ctrl = this;
    
    if ($window.location.search) {
        switch (true) {
        	case (AuthenticationService.parseLocation('logoutSuccess') == 1):
        		ctrl.info = $translate.instant("You have been successfully logged out of the system");
        		break;
        	case (AuthenticationService.parseLocation('login_error') == 1):
	        	ctrl.error = $translate.instant("Invalid Login Attempt. Please try with valid credentials");
        		break;
            case (AuthenticationService.parseLocation('session_timeout') == 1):
                ctrl.error = $translate.instant("You have been logged out because your last session expired.");
                break;
            default:
        		ctrl.error = $translate.instant("An error occured while processing the request. Kindly contact your System Administrator!!!");
        }
    }
};