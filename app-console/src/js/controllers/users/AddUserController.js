/**
 * Add User Controller
 */

angular
    .module('AppConsole')
    .controller('AddUserController', AddUserController);

function AddUserController($translate, UsersManager, RolesManager, $state, growl) {
	var ctrl = this;
    
	RolesManager.loadAllRoles().then(function(roles) {
    	ctrl.roles = roles;
    });
 
    ctrl.saveUser = function() {
        UsersManager.saveUser(ctrl.user).then(function (response) {
            $state.go('Users');
            growl.success($translate.instant('User added'), {title: $translate.instant('Success!')});
        });
    }
}
