/**
 * Edit User Controller
 */

angular
    .module('AppConsole')
    .controller('EditUserController', EditUserController);

function EditUserController($translate, $state, growl, UsersManager, RolesManager, $modal) {
	var ctrl = this;
    var id = $state.params.id;

	RolesManager.loadAllRoles().then(function(roles) {
		ctrl.roles = roles;
    });
	
    ctrl.updateUser = function() {
   		UsersManager.updateUser(id, ctrl.user).then(function(response) {
    		if(ctrl.userName != ctrl.user.userName) {
   				growl.success($translate.instant('User updated'), {title: $translate.instant('Success!')});
   				$state.go('Users', {}, {reload: true});
   	    	} else {
   				SharedManager.openModalToAskUser($translate.instant("Password reset"), $translate.instant("Your Password has been changed. Please re-login"));
   			}
        });
    };
    
    if (id) {
        UsersManager.fetchUser(id).then(function (user) {
            ctrl.user = user.plain();
        });
    }
}
