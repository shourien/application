/**
 * Users Controller
 */

angular
    .module('AppConsole')
    .controller('UsersController', UsersController);

function UsersController(growl, $state, $translate, UsersManager, SharedManager) {
    var ctrl = this;
    ctrl.itemsByPage = UsersManager.getItemsPerPage();
    ctrl.columns = UsersManager.getUsersViewColumns();

    ctrl.callback = function(item) {
    	ctrl.itemsByPage = item;
		UsersManager.setItemsPerPage(item);
    };
    
    ctrl.callServer = function callServer(tableState) {
    	var pagination = tableState.pagination;
    	var start = pagination.start || 0;
        var number = pagination.number || UsersManager.getItemsPerPage();
        UsersManager.setSelectedCriteria(ctrl.filter = UsersManager.getSelectedCriteria() ? UsersManager.getSelectedCriteria() : tableState.search.predicateObject);
	    UsersManager.loadAllUsers(start, number, UsersManager.getSelectedCriteria()).then(function(users) {
	    	ctrl.displayed = users;
	    	tableState.pagination = SharedManager.paginateResponse(tableState.pagination, users, number);
	    });
    };

	ctrl.deleteUser = function(id) {
		var modalInstance = SharedManager.openModalToAskUser( $translate.instant("User Delete?"),  $translate.instant("Are you sure you want to delete the user?"));
	    	modalInstance.result.then(function () {
	    	UsersManager.deleteUser(id).then(function(response) {
	    		growl.success( $translate.instant('User deleted'),{title:  $translate.instant('Success!')});
	    		$state.go('Users', {}, {reload: true});
	        });
	    });
    };
}
