/**
 * Roles Controller
 */

angular
    .module('AppConsole')
    .controller('RolesController', RolesController);

function RolesController(growl, $state, RolesManager, $translate, SharedManager) {
    var ctrl = this;
    ctrl.itemsByPage = RolesManager.getItemsPerPage();
    ctrl.columns = RolesManager.getRolesViewColumns();
    
    ctrl.callback = function(item) {
    	ctrl.itemsByPage = item;
		RolesManager.setItemsPerPage(item);
    };
    
    ctrl.callServer = function callServer(tableState) {
		var pagination = tableState.pagination;
    	var start = pagination.start || 0;
        var number = pagination.number || RolesManager.getItemsPerPage();
        RolesManager.setSelectedCriteria(ctrl.filter = RolesManager.getSelectedCriteria() ? RolesManager.getSelectedCriteria() : tableState.search.predicateObject);
	    RolesManager.loadAllRoles(start, number, RolesManager.getSelectedCriteria()).then(function(tenants) {
	    	ctrl.displayed = tenants;
	    	tableState.pagination = SharedManager.paginateResponse(tableState.pagination, tenants, number);
	    });
   };

	ctrl.deleteRole = function(tenantId) {
		var modalInstance = SharedManager.openModalToAskUser($translate.instant("Role Delete?"), $translate.instant("Are you sure you want to delete the tenant?"));
			modalInstance.result.then(function () {
	    	RolesManager.deleteRole(tenantId).then(function(response) {
	    		growl.success($translate.instant('Role deleted'),{title: $translate.instant('Success!')});
	    		$state.go('Roles', {}, {reload: true});
	        });
	    });
    }
}
