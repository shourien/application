/**
 * Add Role Controller
 */

angular
    .module('AppConsole')
    .controller('AddRoleController', AddRoleController);

function AddRoleController($state, growl, $translate, RolesManager) {
	var ctrl = this;
	ctrl.saveRole = function() {
    	RolesManager.saveRole(ctrl.role).then(function(response) {
    		growl.success($translate.instant('Role added'),{title: $translate.instant('Success!')});
    		$state.go('Roles');
        });
    };
}
