/**
 * Edit Role Controller
 */

angular
    .module('AppConsole')
    .controller('EditRoleController', EditRoleController);

function EditRoleController($state, growl, $translate, RolesManager) {
	var ctrl = this;
    var id = $state.params.id;
    
    ctrl.saveRole = function() {
    	RolesManager.updateRole(ctrl.role).then(function(response) {
    		growl.success($translate.instant('Role updated'), {title: $translate.instant('Success!')});
    		$state.go('Roles');
        });
    };
    
    if (id) {
    	RolesManager.fetchRole(id).then(function(role) {
    		ctrl.role = role;
    	});
    }
}
