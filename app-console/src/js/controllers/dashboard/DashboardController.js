/**
 * Dashboard Controller
 */

angular
    .module('AppConsole')
    .controller('DashboardController', DashboardController);

function DashboardController($scope, $translate, ChartsManager, SharedManager, DashboardManager) {
	var ctrl = this;
	//line chart preparation with highcharts library
	var lineChartNGVar = angular.merge(ChartsManager.getHighchartConfigOption('line'), {
			title: {
				text: $translate.instant('Total devices')
	        },
	        subtitle: {
	            text: $translate.instant('Showing device trend since installation')
	        },
			yAxis: {
				title: {
					text: $translate.instant('Devices')
				}
			},
			series: [{
	        	name: $translate.instant('Devices'),
	        	data: []
	        }]
		});
	//stacked bar chart preparation with highcharts library
    var highChartNGVar = angular.merge(ChartsManager.getHighchartConfigOption('column'), {
	    	title: {
				text: $translate.instant('Job Stats')
	        },
	        subtitle: {
	            text: $translate.instant('Showing Job statistics for past 6 months')
	        },
    		yAxis: {
    			title: {
    				text: $translate.instant('Total Jobs')
    			}
    		}
    	});
    var defaultContainer = [{lineChartNG: lineChartNGVar, graph: 0}, {highChartNG: highChartNGVar, graph: 0}, {tenant: {count: 0}, graph: 1}, {user: {count: 0}, graph: 1 }, {device: {count: 0}, graph: 1}];
    ctrl.container = SharedManager.retrieveValue('position') ? SharedManager.retrieveValue('position') : defaultContainer;

    ctrl.options = {
		containersModel: ctrl.container,
		scope: $scope,
		moves: function(el, container, handle) {
			return handle.classList.contains('handle');
		}
    };
    
    DashboardManager.fetchDashboardData().then(function (cData) {
    	_.find(ctrl.container, function(widget) {return widget.tenant}).tenant.count = cData.data.tenantcount;
    	_.find(ctrl.container, function(widget) {return widget.user}).user.count = cData.data.usercount;
    	_.find(ctrl.container, function(widget) {return widget.device}).device.count = cData.data.devicecount ? cData.data.devicecount : 0;
    	_.find(ctrl.container, function(widget) {return widget.lineChartNG}).lineChartNG.xAxis.categories = cData.data.deviceschart.months;
    	_.find(ctrl.container, function(widget) {return widget.lineChartNG}).lineChartNG.series[0].data = cData.data.deviceschart.devicesCount;
    	_.find(ctrl.container, function(widget) {return widget.highChartNG}).highChartNG.xAxis.categories = cData.data.jobschart.months;
    	_.find(ctrl.container, function(widget) {return widget.highChartNG}).highChartNG.series = cData.data.jobschart.jobResultsByStatus;
	});
    
    $scope.$on('dragulardrop', function() {
    	SharedManager.storeValue('position', ctrl.container)
    });
}
