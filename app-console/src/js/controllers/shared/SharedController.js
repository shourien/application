/**
 * Globally Shared Controller
 */

angular
    .module('AppConsole')
    .controller('SharedController', SharedController);

function SharedController($scope, $state) {
	
	$scope.transition = function(location, parameters) {
		$state.go(location, parameters);
	};
}
