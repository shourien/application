/**
 * Modal Instance Controller
 */

angular
    .module('AppConsole')
    .controller('ModalInstanceController', ModalInstanceController);

function ModalInstanceController($modalInstance, header, body) {
	var ctrl = this;
	ctrl.header = header;
	ctrl.body = body;
	
	ctrl.confirm = function() {
		$modalInstance.close();
	};

	ctrl.cancel = function() {
		$modalInstance.dismiss('cancel');
	};
}