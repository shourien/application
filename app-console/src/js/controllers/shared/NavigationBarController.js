/**
 * Navigation Bar Controller
 */

angular.module('AppConsole')
    .controller('NavigationBarController', NavigationBarController);

function NavigationBarController($location, $modal, SharedManager) {
    var ctrl = this;
	var currentNode = null;
    
    ctrl.captureEventId = function($event) {
    	currentNode = (currentNode == $event.currentTarget.id) ? null : $event.currentTarget.id;
    	return currentNode;
    };
    
    ctrl.isCollapse = function(eventId) {
    	if(eventId == null) return true;
    	return !(currentNode != null && currentNode == eventId);
    }
    
    ctrl.navClass = function (page) {
        var currentRoute = $location.path().substring(1);
        return _.some(page, function(value) {return currentRoute.indexOf(value) > -1}) ? 'active' : '';
    };

    ctrl.toggleSidebar = function(toggle) {
        SharedManager.storeValue('toggle', toggle);
    };
}
