/**
 * Master Controller
 */

angular.module('AppConsole')
    .controller('MasterController', MasterController);

function MasterController($scope, SharedManager, SelfManager, $location, $window, $translate, Restangular, growl) {
    var mobileView = 992;
    var ctrl = this;
    
    ctrl.getWidth = function getWidth() {
        return window.innerWidth;
    };
    ctrl.sidebar = {name: "sidebar.html", url: "templates/shared/sidebar.html"};
	ctrl.header = {name: "header.html", url: "templates/shared/header.html"};
	
	SelfManager.fetchSelfInfo().then(function(self) {
		ctrl.userName = self.name;
	});

    $scope.$watch(ctrl.getWidth, function(newValue, oldValue) {
        if (newValue >= mobileView) {
        	ctrl.toggle = (SharedManager.retrieveValue('toggle') == null || SharedManager.retrieveValue('toggle')) ? true : false;
        } else {
        	ctrl.toggle = false;
        }
    });

    window.onresize = function() {
        $scope.$apply();
    };

	Restangular.setErrorInterceptor(function(response, deferred, responseHandler) {
		if(response.status != 404 && response.data.localizedMessage) {
			growl.error($translate.instant(response.data.localizedMessage), {title: $translate.instant('Error!')});
		}
	});

	Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
		if(response.headers('Content-Type') === 'text/html') {  //preferably the server should send the response code for session timeout, But workaround in its place
			$window.location.href = $location.absUrl().indexOf('?') > -1 ?  
									$location.absUrl().substring(0, $location.absUrl().indexOf('?') + 1) + 'session_timeout=1' : 
									$location.absUrl().substring(0, $location.absUrl().indexOf('#')) + 'home.html?session_timeout=1';
		}
    	if (operation === 'getList') {
    		var formatResponse = data.aaData;
    		formatResponse.iTotalRecords = data.iTotalRecords;
    		return formatResponse;
        }
        return data;
    });

}