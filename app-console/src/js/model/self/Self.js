/**
 * Self Model
 */

angular
    .module('AppConsole')
    .factory('Self', Self);

function Self(Restangular) {
	return Restangular.service('self');
}
