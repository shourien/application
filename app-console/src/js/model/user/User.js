/**
 * Users Model
 */

angular
    .module('AppConsole')
    .factory('User', User);

function User(Restangular) {
	return Restangular.service('user');
}
