/**
 * Dashboard Model
 */

angular
    .module('AppConsole')
    .factory('Dashboard', Dashboard);

function Dashboard(Restangular) {
	return Restangular.service('dashboard');
}
