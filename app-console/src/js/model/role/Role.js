/**
 * Tenants Model
 */

angular
    .module('AppConsole')
    .factory('Role', Role);

function Role(Restangular) {
	return Restangular.service('role');
}
