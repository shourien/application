/**
 * Self Service
 */

angular
    .module('AppConsole')
    .service('SelfManager', SelfManager);

function SelfManager(Self) {
	var selfManager = {
		fetchSelfInfo: function() {
			return Self.one().get();
		}
	};
	return selfManager;
};
