/**
 * Chart Functions Services
 */
angular
    .module('AppConsole')
    .service('ChartsManager', ChartsManager);

function ChartsManager() {
	var ChartsManager = {
		getHighchartConfigOption: function(type) {
        	switch(type) {
        		case "column":
	            	return {
        				title: {
        		            text: '',
        		            align: 'left',
        					style: {
        				         color: '#3671C0'
        				    }
        		        },
        		        subtitle: {
        		            text: '',
        		            align: 'left'
        		        },
        		        credits: {
        		            enabled: false
        		        },
        		        xAxis: {
        		            categories: []
        		        },
        		        yAxis: {
        		            min: 0        		            
        		        },
        		        options: {
        		        	chart: {
        		        		type: 'column',
        		        		style: {
            		                fontFamily: 'Open Sans'
            		            },
            		            options3d: {
            		                enabled: true,
            		                alpha: 15,
            		                beta: 15,
            		                viewDistance: 25,
            		                depth: 40
            		            }
        		        	},
        		        	exporting: {
	                            buttons: {
	                            	contextButton: {
	                            		x: 2,
	                            		y: 0
	                            	}
	                            }
	                        },
        		        	legend: {
        		        		align: 'center',
        		        		verticalAlign: 'bottom',
        		        		floating: false,
        		        		backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
        		        		borderColor: '#CCC',
        		        		borderWidth: 1,
        		        		shadow: false
        		        	},
        		        	tooltip: {
        		        		formatter: function() {
        		        			return '<b>'+ this.x +'</b><br/>'+
        		        			this.series.name +': '+ this.y +'<br/>'+
        		        			'Total: '+ this.point.stackTotal;
        		        		}
        		        	},
        		        	plotOptions: {
        		        		column: {
        		        			stacking: 'normal'
        		        		}
        		        	}
        		        },
        		        series: []
        		    }
	            break;
        		case "line":
        			return {
            			title: {
            	            text: '',
            	            align: 'left',
        					style: {
        				         color: '#3671C0'
        				    }
            	        },
            	        subtitle: {
        		            text: '',
        		            align: 'left'
        		        },
            	        credits: {
            	            enabled: false
            	        },
            	        xAxis: {
            	            categories: []
            	        },
            	        yAxis: {
            	            min: 0
            	        },
            	        options: {
            	        	chart: {
            	        		type: 'line',
            	        		zoomType: 'x',
            	        		style: {
            		                fontFamily: 'Open Sans'
            		            }
            	        	},
            	        	exporting: {
	                            buttons: {
	                            	contextButton: {
	                            		x: 2
	                            	}
	                            }
	                        },
            	        	legend: {
            	        		align: 'center',
            	        		verticalAlign: 'bottom',
            	        		floating: false,
            	        		backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || 'white',
            	        		borderColor: '#CCC',
            	        		borderWidth: 1,
            	        		shadow: false
            	        	},
            	        	tooltip: {
            	        		formatter: function() {
            	        			return '<b>'+ this.x +'</b><br/>'+
            	        			this.series.name +': '+ this.y;
            	        		}
            	        	},
            	        	plotOptions: {
            	        		column: {
            	        			stacking: 'normal'
            	        		}
            	        	}
            	        },
            	        series: []
        			}
        		break;
        	}
        }
	};
	return ChartsManager;
};