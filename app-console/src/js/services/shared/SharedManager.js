/**
 * Custom Shared Functions Services
 */

angular
    .module('AppConsole')
    .service('SharedManager', SharedManager);

function SharedManager($modal) {
    var SharedManager = {
    	getDefaultPageSize: function() {
    		return 10;
    	},
    	retrieveValue: function(key) {
			return JSON.parse(localStorage.getItem(key));
		},
		storeValue: function(key, value) {
			localStorage.setItem(key, JSON.stringify(value));
		},
		clearValue: function(key) {
			localStorage.removeItem(key);
		},
		paginateResponse: function(pagination, response, number) {
			return angular.merge(pagination, {
				start: (response.start || response.start === 0) ? response.start : pagination.start,
	    		numberOfPages: Math.ceil(response.iTotalRecords / number),
	    		totalItemCount: response.iTotalRecords
	    	});
		},
		baseModalAttributes: function(controllerFunc, template, size, resolveAttributes) {
			return {
				animation: true,
				backdrop: 'static',
				controller: controllerFunc,
				controllerAs: 'mc',
				templateUrl: template,
				size: size || 'lg',
				resolve: {
					attributes : function() {
						return resolveAttributes;
					}
				}
			};
		},
        openModalToAskUser: function(header, body){
            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'templates/shared/modal.html',
                controller: 'ModalInstanceController',
                controllerAs: 'mc',
                backdrop: 'static',
                size: 'sm',
                resolve: {
                    header: function () {
                        return header;
                    },
                    body: function() {
                        return body;
                    }
                }
            });
            return modalInstance;
        }
    };
    return SharedManager;
};
