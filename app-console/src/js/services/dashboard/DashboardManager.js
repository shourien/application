/**
 * Dashboard Services
 */

angular
    .module('AppConsole')
    .service('DashboardManager', DashboardManager); 

function DashboardManager($http, Dashboard) {
	var dashboardManager = {
	    /* Use this function in order to get the dashboard data */
		fetchDashboardData: function() {
			return $http.get('templates/dashboard/dashboard.json');
		}
	};
	return dashboardManager;
};
