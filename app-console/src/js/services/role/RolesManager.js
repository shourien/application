/**
 * Roles Services
 */

angular
    .module('AppConsole')
    .service('RolesManager', RolesManager);

function RolesManager(Role, SharedManager) {
	var tenantsItemsPerPage = SharedManager.getDefaultPageSize();
	var selectedCriteria = null;
	var tenantsViewColumns = {
		colRolesDescription: true,
		colRolesCreated: true,
		colRolesUpdated: true
	};
	var rolesManager = {
	    saveRole: function(tenantData) {
	    	return Role.post(tenantData);
	    },
	    updateRole: function(tenantData) {
	    	return Role.one(tenantData.id).customPUT(tenantData);
	    },
	    /* Public Methods */
	    /* Use this function in order to get instances of all the tenants */
	    loadAllRoles: function(pageStart, pageSize, predicate) {
	        return Role.getList({iDisplayStart:pageStart, iDisplayLength:!pageSize?-1:pageSize, sSearch:predicate?predicate.name : null});
	    },
	    /* Use this function in order to get the tenant instance */
		fetchRole: function(id){
			return Role.one(id).get();
		},
		/* Delete Data */
	    deleteRole: function(id) {
	    	return Role.one(id).remove();
	    },
		setItemsPerPage: function(value){
			tenantsItemsPerPage = value;
		},
		getItemsPerPage: function(){
			return tenantsItemsPerPage;
		},
		getRolesViewColumns: function() {
			return tenantsViewColumns;
		},
		getSelectedCriteria: function() {
			return selectedCriteria;
		},
		setSelectedCriteria: function(criteria) {
			selectedCriteria = criteria;
		}
	};
	return rolesManager;
};
