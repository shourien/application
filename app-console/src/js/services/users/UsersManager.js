/**
 * Users Services
 */

angular
    .module('AppConsole')
    .service('UsersManager', UsersManager);

function UsersManager(User, SharedManager) {
	var usersItemsPerPage = SharedManager.getDefaultPageSize();
	var selectedCriteria = null;
	var usersViewColumns = {
		colFirstName: true,
		colLastName: true,
		colAddress: true,
		colUsersRole: true,
		colUsersCreated: true,
		colUsersUpdated: true
	};
	var usersManager = {
	    saveUser: function(userData) {
	    	return User.post(userData);
	    },
	    updateUser: function(id, userData) {
	    	return User.one(id.toString()).customPUT(userData);
	    },
	    loadAllUsers: function(pageStart, pageSize, predicate) {
	        return User.getList({iDisplayStart:pageStart, iDisplayLength:!pageSize?-1:pageSize, sSearch:predicate?predicate.name : null});
	    },
		fetchUser: function(id) {
			return User.one(id.toString()).get();
		},
	    deleteUser: function(id) {
	    	return User.one(id.toString()).remove();
	    },
		getItemsPerPage: function(){
			return usersItemsPerPage;
		},
		setItemsPerPage: function(value){
			usersItemsPerPage = value;
		},
		getUsersViewColumns: function() {
			return usersViewColumns;
		},
		getSelectedCriteria: function() {
			return selectedCriteria;
		},
		setSelectedCriteria: function(criteria) {
			selectedCriteria = criteria;
		}
	};
	return usersManager;
};
