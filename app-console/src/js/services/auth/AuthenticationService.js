﻿/**
 * Authentication Services
 */
angular
	.module('Authentication')
    .service('AuthenticationService', AuthenticationService);

function AuthenticationService($window) {
	var service = {};
	service.parseLocation = function(name) {
		return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)')
		.exec($window.location.search)||[,""])[1].replace(/\+/g, '%20')) || null
	};
	return service;
};