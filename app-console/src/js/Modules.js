//declare login module
angular.module('Authentication', ['pascalprecht.translate',
								  'ui.router'
								]);
// declare modules
angular.module('AppConsole', ['ui.bootstrap', 
                              'ui.router', 
                              'ngAnimate',
                              'highcharts-ng',
                              'ncy-angular-breadcrumb',
                              'smart-table', 
                              'underscore', 
                              'restangular',
                              'angular-loading-bar',
                              'angular-growl',
                              'pascalprecht.translate',
                              'dragularModule'
                          	]);