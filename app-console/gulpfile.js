var gulp = require('gulp'),
    usemin = require('gulp-usemin'),
    connect = require('gulp-connect'),
    watch = require('gulp-watch'),
    cleanCSS = require('gulp-clean-css'),
    minifyJs = require('gulp-uglify'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    htmlmin = require('gulp-htmlmin'),
    ngAnnotate = require('gulp-ng-annotate'),
    stripJsonComments = require('gulp-strip-json-comments'),
    iife = require('gulp-iife');

var paths = {
    scripts: ['src/js/**/*.js','!src/js/AuthRouter.js', '!src/js/**/auth/*'],
    login_scripts: ['src/js/Modules.js', 'src/js/AuthRouter.js', 'src/js/**/auth/*.js'],
    templates: 'src/templates/**/*.*',
    index: 'src/home.html',
    login: 'src/login.html',
    files: ['src/error.html'],
    images: 'src/img/**/*.*',
    mocks: 'src/rest-proxy/**/*',
    locales:'src/locales/*',
    bootstrap_fonts: 'node_modules/bootstrap/fonts/*.*',
};

/**
 * Handle bootstrap components from index
 */
gulp.task('usemin', function() {
    return gulp.src(paths.index)
        .pipe(usemin({
            js: [minifyJs(), 'concat'],
            css: [cleanCSS({keepSpecialComments: 0}), 'concat']
        }))
        .pipe(gulp.dest('target/app-console/'));
});

gulp.task('usemin-login', function() {
    return gulp.src(paths.login)
        .pipe(usemin({
            js: [minifyJs(), 'concat'],
            css: [cleanCSS({keepSpecialComments: 0}), 'concat']
        }))
        .pipe(gulp.dest('target/app-console/'));
});

/**
 * Copy assets
 */
gulp.task('build-assets', ['copy-bootstrap_fonts']);

gulp.task('copy-bootstrap_fonts', function() {
    return gulp.src(paths.bootstrap_fonts)
        .pipe(rename({
            dirname: '/fonts'
        }))
        .pipe(gulp.dest('target/app-console/lib'));
});

/**
 * Handle custom files
 */
gulp.task('build-custom', ['custom-images', 'custom-js', 'custom-js-login', 'custom-templates', 'custom-files', 'custom-locales']);

gulp.task('custom-images', function() {
    return gulp.src(paths.images)
        .pipe(gulp.dest('target/app-console/img'));
});

gulp.task('custom-js', function() {
    return gulp.src(paths.scripts)
    	.pipe(ngAnnotate())
    	.pipe(iife())
        .pipe(minifyJs())
        .pipe(concat('appl.min.js'))
        .pipe(gulp.dest('target/app-console/js'));
});

gulp.task('custom-js-login', function() {
    return gulp.src(paths.login_scripts)
    	.pipe(ngAnnotate())
    	.pipe(iife())
        .pipe(minifyJs())
        .pipe(concat('login.min.js'))
        .pipe(gulp.dest('target/app-console/js'));
});

gulp.task('custom-templates', function() {
    return gulp.src(paths.templates)
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest('target/app-console/templates'));
});

gulp.task('custom-locales',function(){
	return gulp.src(paths.locales)
        .pipe(stripJsonComments())
		.pipe(gulp.dest('target/app-console/locales'));
});

gulp.task('custom-files', function() {
    return gulp.src(paths.files)
        .pipe(gulp.dest('target/app-console/'));
});

gulp.task('build-mocks', function() {
    return gulp.src(paths.mocks)
        .pipe(gulp.dest('target/app-console/rest-proxy'));
});

/**
 * Watch custom files
 */
gulp.task('watch', function() {
    gulp.watch([paths.scripts], ['custom-js', 'livereload']);
    gulp.watch([paths.templates], ['custom-templates', 'livereload']);
    gulp.watch([paths.mocks], ['build-mocks', 'livereload']);
    gulp.watch([paths.index], ['usemin', 'livereload']);
});

/**
 * Live reload server
 */

gulp.task('webserver', function() {
    connect.server({
        root: 'target',
        livereload: true,
        port: 8888,
        middleware: function(connect, options) {
        	return [
        	    function(req, res, next) {
        	    	if(req.url.indexOf('rest-proxy') !== -1) {
        	    		req.url = req.url.indexOf('?') !== -1 ? req.url.replace('?', '.json?') : (req.url + '.json');
        	    	}
        	    	req.method = 'GET'; //Convert all requests to GET
				    return next();
        	    }
        	];
        }
    });
});

gulp.task('livereload', function() {
    gulp.src(['target/app-console/**/*.*'])
        .pipe(connect.reload());
});

/**
 * Gulp tasks
 */

gulp.task('build', ['usemin', 'usemin-login', 'build-assets', 'build-custom']);
gulp.task('debug', ['usemin', 'build-assets', 'build-custom', 'build-mocks', 'webserver', 'livereload', 'watch']);
gulp.task('default', ['build']);
