package com.myproject.app.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import com.myproject.app.rest.generated.models.Error;

/**
 * Handle 'unexpected' exceptions. Normal, validation exception that need i18n should not end up here.
 */
@Provider
public class ThrowableExceptionMapper implements ExceptionMapper<Throwable> {
	
    private static Logger log = LoggerFactory.getLogger(ThrowableExceptionMapper.class);

    public static Response createErrorResponse(Response.Status status, Error error) {
        return Response.status(status).type("application/json").entity(error).build();
    }

    @Override
    public Response toResponse(Throwable t) {
    	log.debug("Converting unexpected exception", t);
    	Error error = createUnexpectedError(t);
    	return createErrorResponse(Response.Status.INTERNAL_SERVER_ERROR, error);
    }
    
    private Error createUnexpectedError(Throwable t) {
        Error error = new Error();
        error.setCode("UNEXPECTED_ERROR");
        error.setLocalizedMessage(t.getMessage());
        return error;
    }
}
