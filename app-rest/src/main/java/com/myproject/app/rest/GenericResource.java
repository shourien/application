package com.myproject.app.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;

import com.myproject.app.db.GenericPersistenceHandler;
import com.myproject.app.db.GenericQueryHandler;
import com.myproject.app.db.PageInfo;
import com.myproject.app.entities.PersistentObject;
import com.myproject.app.exception.DatabaseException;
import com.myproject.app.rest.exception.ValidationException;
import com.myproject.app.rest.generated.models.Error;
import com.myproject.app.rest.generated.models.Paged;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import static com.myproject.app.rest.ThrowableExceptionMapper.createErrorResponse;

/**
 * Base class for REST resources.
 */
public abstract class GenericResource<ListType, DetailType, InternalType extends PersistentObject, SnapshotType> implements ValidatableResource<DetailType,InternalType> {

	protected static final String RESOURCE_NOT_FOUND_MSG = "Resource not found";

	protected static final String RESOURCE_NOT_FOUND = "RESOURCE_NOT_FOUND";

    protected GenericResource() {
    	
    }

    protected abstract GenericQueryHandler<InternalType> getGenericQueryHandler();

    protected abstract GenericPersistenceHandler<InternalType, SnapshotType> getGenericCommandHandler();

    public static int getQueryParameter(UriInfo uriInfo, String name, int defaultValue) {
        String value = uriInfo.getQueryParameters().getFirst(name);
        return value != null ? Integer.parseInt(value) : defaultValue;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response query(@Context UriInfo uriInfo) {
        int iDisplayStart = getQueryParameter(uriInfo, "iDisplayStart", 0);
        int iDisplayLength = getQueryParameter(uriInfo, "iDisplayLength", 10);
        Paged res = new Paged();        
        PageInfo pageInfo = new PageInfo(iDisplayStart, iDisplayLength);
        String nameFilter = uriInfo.getQueryParameters().getFirst("sSearch");
        List<InternalType> all;
        
        if (nameFilter == null || nameFilter.trim().length() == 0) {
            all = getGenericQueryHandler().findAll(pageInfo);
        } else {
            all = getGenericQueryHandler().findByName(nameFilter, pageInfo);
        }
        Optional<Long> count = getGenericQueryHandler().countAll(nameFilter);
        for (InternalType internalType : all) {
            res.addAaDataItem(convertToSummary(internalType));
        }
        res.iTotalDisplayRecords((long) all.size());
        res.iTotalRecords(count.orElse(null));
        return Response.ok(res).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(@Context UriInfo uriInfo, @Context HttpServletRequest request, DetailType detail) {
        try {
            UriBuilder uriBuilder = UriBuilder.fromUri(uriInfo.getRequestUri());
            uriBuilder.path("{id}");
            validate(detail, null, request);
            InternalType newInstance = createInternal(detail);
            Long id = getGenericCommandHandler().persist(newInstance);
            return Response.created(uriBuilder.build(id)).build();
        } catch (ValidationException | DatabaseException e) {
            return buildValidationErrorResponse(e);
        }
    }

    protected Response buildValidationErrorResponse(Exception e) {
        return createErrorResponse(Response.Status.BAD_REQUEST, new Error().localizedMessage(e.getMessage()));
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") Long id, @Context HttpServletRequest request, DetailType detail) {
        Optional<InternalType> res = getGenericQueryHandler().findById(id);
        if (res.isPresent()) {
            try {
                validate(detail, res.get(), request);
                updateInternalFromDetail(res.get(), detail);
                getGenericCommandHandler().updated(res.get());
                return Response.ok().build();
            } catch (ValidationException | DatabaseException e) {
                return buildValidationErrorResponse(e);
            }
        } else {
            return notFound();
        }
    }
    
    public static Response notFound() {
        Error error = new Error();
        error.setLocalizedMessage(RESOURCE_NOT_FOUND_MSG);
        error.setCode(RESOURCE_NOT_FOUND);
        return createErrorResponse(Response.Status.NOT_FOUND, error);
    }

    @DELETE
    @Path("{id}")
    public Response delete(@PathParam("id") Long id) {
        Optional<InternalType> res = getGenericQueryHandler().findById(id);
        if (res.isPresent()) {
            getGenericCommandHandler().delete(res.get());
            return Response.noContent().build();
        } else {
            return notFound();
        }
    }

    @DELETE
    public Response deleteByName(@QueryParam("name") String name) {
        Optional<InternalType> res = getGenericQueryHandler().findByName(name);
        if (res.isPresent()) {
            getGenericCommandHandler().delete(res.get());
            return Response.ok().build();
        } else {
            return notFound();
        }
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") Long id) {
        Optional<InternalType> res = getGenericQueryHandler().findById(id);
        if (res.isPresent()) {
            DetailType detailType = convertToDetail(res.get());
            return Response.ok().entity(detailType).build();
        } else {
            return notFound();
        }
    }

    protected abstract ListType convertToSummary(InternalType internalType);

    protected abstract DetailType convertToDetail(InternalType internalType);

    protected abstract void updateInternalFromDetail(InternalType internal, DetailType detail) throws ValidationException;

    protected abstract InternalType createInternal(DetailType detail) throws ValidationException;
    
    protected void checkForDuplicate(String name, InternalType internal, Function<String, Optional<InternalType>> finder) throws ValidationException{
        Optional<InternalType> entity = finder.apply(name);
        if(entity.isPresent()){
            if(internal == null || !internal.getId().equals(entity.get().getId())){
                throw new ValidationException("Resource already exists");
            }
        }
    }

    public static <T extends com.myproject.app.rest.generated.models.PersistentObject> T populateRestObject(T rest, PersistentObject internal) {
        rest.setId(internal.getId());
        rest.setCreatedOn(internal.getInserted().getTime());
        rest.setUpdatedOn(internal.getUpdated().getTime());
        return rest;
    }

}
