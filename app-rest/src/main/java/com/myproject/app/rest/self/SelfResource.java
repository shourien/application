package com.myproject.app.rest.self;

import javax.ejb.Stateless;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import com.myproject.app.rest.generated.models.Self;

@Path("/self")
@Stateless
public class SelfResource {
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@Context SecurityContext ctx) {
		Self self = new Self();
        self.setName(ctx.getUserPrincipal().getName());
        return Response.ok(self).build();
	}
}
