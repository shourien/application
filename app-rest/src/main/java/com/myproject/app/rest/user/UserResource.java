package com.myproject.app.rest.user;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;

import com.myproject.app.db.GenericPersistenceHandler;
import com.myproject.app.db.GenericQueryHandler;
import com.myproject.app.entities.RoleEntity;
import com.myproject.app.entities.UserEntity;
import com.myproject.app.management.role.RoleDao;
import com.myproject.app.management.user.UserDao;
import com.myproject.app.rest.GenericResource;
import com.myproject.app.rest.exception.ValidationException;
import com.myproject.app.rest.generated.models.Role;
import com.myproject.app.rest.generated.models.User;
import com.myproject.app.rest.utils.MD5Generator;

@Path("/user")
@Stateless
@PermitAll
public class UserResource extends GenericResource<User, User, UserEntity, Long> {

	@EJB(beanInterface = UserDao.class)
    private UserDao userDao;
	
	@EJB(beanInterface = RoleDao.class)
	private RoleDao roleDao;
	
	@Override
	protected GenericQueryHandler<UserEntity> getGenericQueryHandler() {
		return userDao;
	}

	@Override
	protected GenericPersistenceHandler<UserEntity, Long> getGenericCommandHandler() {
		return userDao;
	}

	@Override
	protected User convertToSummary(UserEntity internalType) {
		return convertToDetail(internalType);
	}

	@Override
	protected User convertToDetail(UserEntity internalType) {
		User userDetail = new User();
		userDetail.setId(internalType.getId());
		userDetail.setUserName(internalType.getLogin());
		userDetail.setFirstName(internalType.getFirstName());
		userDetail.setLastName(internalType.getLastName());
		userDetail.setAddress(internalType.getPrimAddress());
		userDetail.setRole(internalType.getRole() != null ? 
				new Role().id(internalType.getRole().getId()).roleName(internalType.getRole().getRoleName()) : null);
		userDetail.setCreatedOn(internalType.getInserted() != null ? internalType.getInserted().getTime() : null);
		userDetail.setUpdatedOn(internalType.getUpdated() != null ? internalType.getUpdated().getTime() : null);
		return userDetail;
	}

	@Override
	protected void updateInternalFromDetail(UserEntity internal, User detail) throws ValidationException {
		Optional.ofNullable(detail.getFirstName()).ifPresent(internal::setFirstName);
		Optional.ofNullable(detail.getLastName()).ifPresent(internal::setLastName);
		if(detail.getPassword() != null) {
			internal.setPassword(encodePassword(detail.getPassword()));
		}
		Optional.ofNullable(detail.getAddress()).ifPresent(internal::setPrimAddress);
		internal.setRole(new RoleEntity(detail.getRole().getId()));		
	}

	@Override
	protected UserEntity createInternal(User detail) throws ValidationException {
		UserEntity entity = new UserEntity();
		entity.setFirstName(detail.getFirstName());
		entity.setLastName(detail.getLastName());
		entity.setLogin(detail.getUserName());
		entity.setPassword(encodePassword(detail.getPassword()));
		entity.setPrimAddress(detail.getAddress());
		entity.setRole(new RoleEntity(detail.getRole().getId()));
		return entity;
	}
	
	@Override
	public void validate(User detail, UserEntity internal, HttpServletRequest request) throws ValidationException {
		Optional.ofNullable(detail.getUserName())
				.map(String::trim)
				.filter(value -> value.length() > 0)
				.orElseThrow(() -> new ValidationException("empty.invalid.loginName"));
				
		if(internal == null) {
			Optional.ofNullable(detail.getPassword())
					.orElseThrow(() -> new ValidationException("empty.invalid.password"));
		}
		
		Optional.ofNullable(detail.getRole())
				.map(Role::getId)
				.map(roleDao::findById)
				.filter(Optional::isPresent)
				.orElseThrow(() -> new ValidationException("invalid.role"));
	}
	
	private String encodePassword(String plainPassword) throws ValidationException {
		try {
			return MD5Generator.encodeString(plainPassword);
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			throw new ValidationException("password.encryption.error");
		}
	}
}
