package com.myproject.app.rest;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.myproject.app.rest.role.RoleResource;
import com.myproject.app.rest.self.SelfResource;
import com.myproject.app.rest.user.UserResource;

public class ApplicationConfig extends Application {
	public Set<Class<?>> getClasses() {
        return new HashSet<Class<?>>(Arrays.asList(UserResource.class, RoleResource.class, SelfResource.class));
	}
}