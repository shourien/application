package com.myproject.app.rest.role;

import java.util.Optional;

import javax.annotation.security.PermitAll;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Path;

import com.myproject.app.db.GenericPersistenceHandler;
import com.myproject.app.db.GenericQueryHandler;
import com.myproject.app.entities.RoleEntity;
import com.myproject.app.management.role.RoleDao;
import com.myproject.app.rest.GenericResource;
import com.myproject.app.rest.exception.ValidationException;
import com.myproject.app.rest.generated.models.Role;

@Path("/role")
@Stateless
@PermitAll
public class RoleResource extends GenericResource<Role, Role, RoleEntity, Long> {

	@EJB(beanInterface = RoleDao.class)
	private RoleDao roleDao;
	
	@Override
	protected GenericQueryHandler<RoleEntity> getGenericQueryHandler() {
		return roleDao;
	}

	@Override
	protected GenericPersistenceHandler<RoleEntity, Long> getGenericCommandHandler() {
		return roleDao;
	}

	@Override
	protected Role convertToSummary(RoleEntity internalType) {
		return convertToDetail(internalType);
	}

	@Override
	protected Role convertToDetail(RoleEntity internalType) {
		Role detail = new Role();
		detail.setId(internalType.getId());
		detail.setRoleName(internalType.getRoleName());
		detail.setDescription(internalType.getDescription());
		detail.createdOn(internalType.getInserted() != null ? internalType.getInserted().getTime() : null);
		detail.updatedOn(internalType.getUpdated() != null ? internalType.getUpdated().getTime() : null);
		return detail;
	}

	@Override
	protected void updateInternalFromDetail(RoleEntity internal, Role detail) throws ValidationException {
		internal.setRoleName(detail.getRoleName());
		internal.setDescription(detail.getDescription());
	}

	@Override
	protected RoleEntity createInternal(Role detail) throws ValidationException {
		RoleEntity roleEntity = new RoleEntity();
		roleEntity.setRoleName(detail.getRoleName());
		roleEntity.setDescription(detail.getDescription());
		return roleEntity;
	}
	
	@Override
	public void validate(Role detail, RoleEntity internal, HttpServletRequest request) throws ValidationException {
		Optional.ofNullable(detail.getRoleName())
				.map(String::trim)
				.filter(name -> name.length() > 0)
				.orElseThrow(() -> new ValidationException("empty.invalid.roleName"));
	}

}
