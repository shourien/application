package com.myproject.app.rest.exception;

public class ValidationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5022090340623470656L;
	
	private String message;

	public ValidationException(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}

}
