package com.myproject.app.rest;

import javax.servlet.http.HttpServletRequest;

import com.myproject.app.rest.exception.ValidationException;

public interface ValidatableResource<DetailType,InternalType> {

    default void validate(DetailType detail, InternalType internal, HttpServletRequest request) throws ValidationException {
        //Override if resource needs validation
    }
}
