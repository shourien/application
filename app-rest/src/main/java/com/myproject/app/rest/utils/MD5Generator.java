package com.myproject.app.rest.utils;

import org.apache.commons.codec.binary.Base64;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5Generator {

	public static String encodeString(String key) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest md = MessageDigest.getInstance("MD5");
		md.update(key.getBytes("UTF-8"));
	    byte[] theDigest = md.digest();
		return Base64.encodeBase64String(theDigest).trim();
	}
}