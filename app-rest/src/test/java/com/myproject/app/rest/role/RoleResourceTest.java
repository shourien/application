package com.myproject.app.rest.role;

import static org.junit.Assert.*;

import java.net.URISyntaxException;
import java.util.List;
import java.util.Map.Entry;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.jboss.resteasy.mock.MockHttpRequest;
import org.junit.Test;

import com.myproject.app.AbstractRestManager;
import com.myproject.app.rest.generated.models.Error;
import com.myproject.app.rest.generated.models.Paged;
import com.myproject.app.rest.generated.models.Role;
import com.myproject.app.rest.generated.models.User;
import com.myproject.app.rest.user.UserResource;

public class RoleResourceTest extends AbstractRestManager {
	
	private UserResource userResource;

	private RoleResource roleResource;
	
	public RoleResourceTest() throws NamingException {
		userResource = (UserResource) context.lookup("java:global/app-rest/UserResource");
		roleResource = (RoleResource) context.lookup("java:global/app-rest/RoleResource");
	}
	
	@Test
	public void testListingOfAllRoles() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role");
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertNotNull(page.getITotalDisplayRecords());
		assertNotNull(page.getITotalRecords());
		assertTrue(isRolePresent(page, "Hand Of The King"));
	}
	
	@Test
	public void testListingOfPaginatedRole() throws URISyntaxException {
		int pageSize = 2;
		MockHttpRequest request = MockHttpRequest.get("/role?iDisplayStart=0&iDisplayLength=" + pageSize);
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertEquals(page.getAaData().size(), page.getITotalDisplayRecords());
		assertEquals(pageSize, page.getITotalDisplayRecords());
		assertNotNull(page.getITotalDisplayRecords());
		assertNotNull(page.getITotalRecords());
		assertTrue(page.getITotalRecords() > pageSize);
	}
	
	@Test
	public void testPaginatingTillEndOfThePage() throws URISyntaxException {
		int pageSize = 3;
		int currentPage = 0;
		long totalSize = 0;
		while(currentPage * pageSize <= totalSize) {
			MockHttpRequest request = MockHttpRequest.get("/role?iDisplayStart=" + currentPage * pageSize  + "&iDisplayLength=" + pageSize);
			Response response = roleResource.query(request.getUri());
			assertEquals(HttpServletResponse.SC_OK, response.getStatus());
			Paged page = (Paged) response.getEntity();
			totalSize = page.getITotalRecords();
			currentPage += 1;
			assertNotNull(page.getITotalDisplayRecords());
			assertNotNull(page.getITotalRecords());
		}
		assertEquals(Math.round(totalSize/pageSize), currentPage - 1);
	}
	
	@Test
	public void testSearchRoleByName() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role?sSearch=Knight");
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertNotNull(page.getITotalDisplayRecords());
		assertEquals(page.getITotalRecords(), 1);
		assertTrue(isRolePresent(page, "Knight"));
	}
	
	@Test
	public void testSearchRoleByMatchingPattern() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role?sSearch=Lord%25");
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertNotNull(page.getITotalDisplayRecords());
		assertNotNull(page.getITotalRecords());
		assertTrue(isRolePresent(page, "Lord Of Winterfell"));
	}
	
	@Test
	public void testSearchRoleById() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role?sSearch=Knight");
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		Role role = findFirstElement(page, Role.class);
		assertEquals(((Role)roleResource.get(role.getId()).getEntity()).getRoleName(), role.getRoleName());
	}
	
	@Test
	public void testCreateNewRole() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role");
		Role role = new Role().roleName("Grand Maester");
		Response created = roleResource.create(request.getUri(), null, role);
		assertEquals(HttpServletResponse.SC_CREATED, created.getStatus());
		Entry<String, List<Object>> location = created.getHeaders()
														.entrySet()
														.stream()
														.filter(predicate -> predicate.getKey().equals("Location"))
														.findAny()
														.get();
		assertTrue(location.getValue().stream().map(String::valueOf).anyMatch(value -> value.contains("/role/")));
	}
	
	@Test
	public void testCreateRoleWithNullName() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role");
		Role role = new Role();
		Response error = roleResource.create(request.getUri(), null, role);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("empty.invalid.roleName", ((Error) error.getEntity()).getLocalizedMessage()); 
	}
	
	@Test
	public void testCreateRoleWithEmptyName() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role");
		Role role = new Role().roleName("      ");
		Response error = roleResource.create(request.getUri(), null, role);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("empty.invalid.roleName", ((Error) error.getEntity()).getLocalizedMessage()); 
	}
	
	@Test
	public void testUpdateRole() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role?sSearch=3+Eyed+Raven");
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		Role role = findFirstElement(page, Role.class);
		
		role.setRoleName("Three Eyed Raven");
		Response roleResponse = roleResource.update(role.getId(), null, role);
		assertEquals(HttpServletResponse.SC_OK, roleResponse.getStatus());
		Role roleById = (Role) roleResource.get(role.getId()).getEntity();
		assertTrue(roleById.getRoleName().equals("Three Eyed Raven"));
	}
	
	@Test
	public void testDeleteRole() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/role?sSearch=Night+King");
		Response response = roleResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		Role role = findFirstElement(page, Role.class);
		
		Response deleteResponse = roleResource.delete(role.getId());
		assertEquals(HttpServletResponse.SC_NO_CONTENT, deleteResponse.getStatus());
		Error error = (Error) roleResource.get(role.getId()).getEntity();
		assertEquals("RESOURCE_NOT_FOUND", error.getCode());
	}
	
	@Test
	public void testDeleteRoleByName() throws URISyntaxException {
		Response response = roleResource.deleteByName("Lord Commander");
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		
		MockHttpRequest request = MockHttpRequest.get("/role?sSearch=Lord+Commander");
		Paged page = (Paged) roleResource.query(request.getUri()).getEntity();
		assertTrue(page.getITotalRecords().equals(0L));
	}
	
	@Test
	public void testDeleteRoleAssociatedWithUser() throws URISyntaxException {
		Role role = associateRoleToUser();
		Response deleteResponse = roleResource.delete(role.getId());
		assertEquals(HttpServletResponse.SC_NO_CONTENT, deleteResponse.getStatus());
		Error error = (Error) roleResource.get(role.getId()).getEntity();
		assertEquals("RESOURCE_NOT_FOUND", error.getCode());
		
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Jon");
		Response userResponse = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, userResponse.getStatus());
		Paged page = (Paged) userResponse.getEntity();
		assertEquals(0, page.getAaData().size());
	}
	
	private boolean isRolePresent(Paged page, String roleName) {
		return page.getAaData()
					.stream()
					.map(Role.class::cast)
					.map(Role::getRoleName)
					.anyMatch(roleName::contains);
	}
	
	private Role associateRoleToUser() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Jon");
		Response userResponse = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, userResponse.getStatus());
		Paged page = (Paged) userResponse.getEntity();
		User user = findFirstElement(page, User.class);
		
		MockHttpRequest roleRequest = MockHttpRequest.get("/role?sSearch=King+Of+The+North");
		Response roleResponse = roleResource.query(roleRequest.getUri());
		assertEquals(HttpServletResponse.SC_OK, roleResponse.getStatus());
		Paged rolePage = (Paged) roleResponse.getEntity();
		Role role = findFirstElement(rolePage, Role.class);
		
		user.setRole(role);
		Response response = userResource.update(user.getId(), null, user);
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		User userById = (User) userResource.get(user.getId()).getEntity();
		assertTrue(userById.getRole().getId().equals(role.getId()));
		return role;
	}
	
}
