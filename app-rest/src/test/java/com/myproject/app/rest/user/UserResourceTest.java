package com.myproject.app.rest.user;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.junit.Test;
import org.jboss.resteasy.mock.MockHttpRequest;

import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map.Entry;

import com.myproject.app.AbstractRestManager;
import com.myproject.app.rest.generated.models.Paged;
import com.myproject.app.rest.generated.models.Role;
import com.myproject.app.rest.generated.models.User;
import com.myproject.app.rest.generated.models.Error;
import com.myproject.app.rest.role.RoleResource;

import static org.junit.Assert.*;

public class UserResourceTest extends AbstractRestManager {
	
	private UserResource userResource;
	
	private RoleResource roleResource;
	
	public UserResourceTest() throws NamingException {
		userResource = (UserResource) context.lookup("java:global/app-rest/UserResource");
		roleResource = (RoleResource) context.lookup("java:global/app-rest/RoleResource");
	}
	
	@Test
	public void testListingOfAllUsers() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		Response response = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertNotNull(page.getITotalDisplayRecords());
		assertNotNull(page.getITotalRecords());
		assertTrue(isUserPresent(page, "Eddard"));
	}
	
	@Test
	public void testListingOfPaginatedUser() throws URISyntaxException {
		int pageSize = 2;
		MockHttpRequest request = MockHttpRequest.get("/user?iDisplayStart=0&iDisplayLength=" + pageSize);
		Response response = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertEquals(page.getAaData().size(), page.getITotalDisplayRecords());
		assertEquals(pageSize, page.getITotalDisplayRecords());
		assertNotNull(page.getITotalDisplayRecords());
		assertNotNull(page.getITotalRecords());
		assertTrue(page.getITotalRecords() > pageSize);
	}
	
	@Test
	public void testPaginatingTillEndOfThePage() throws URISyntaxException {
		int pageSize = 3;
		int currentPage = 0;
		long totalSize = 0;
		while(currentPage * pageSize <= totalSize) {
			MockHttpRequest request = MockHttpRequest.get("/user?iDisplayStart=" + currentPage * pageSize  + "&iDisplayLength=" + pageSize);
			Response response = userResource.query(request.getUri());
			assertEquals(HttpServletResponse.SC_OK, response.getStatus());
			Paged page = (Paged) response.getEntity();
			totalSize = page.getITotalRecords();
			currentPage += 1;
			assertNotNull(page.getITotalDisplayRecords());
			assertNotNull(page.getITotalRecords());
		}
		assertEquals(Math.round(totalSize/pageSize), currentPage - 1);
	}
	
	@Test
	public void testSearchUserByName() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Sansa");
		Response response = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertNotNull(page.getITotalDisplayRecords());
		assertEquals(page.getITotalRecords(), 1);
		assertTrue(isUserPresent(page, "Sansa"));
	}
	
	@Test
	public void testSearchUserByMatchingPattern() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Ober%25");
		Response response = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		assertNotNull(page.getITotalDisplayRecords());
		assertNotNull(page.getITotalRecords());
		assertTrue(isUserPresent(page, "Oberyn"));
	}
	
	@Test
	public void testSearchUserById() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Sansa");
		Response response = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		User user = findFirstElement(page, User.class);
		assertEquals(((User)userResource.get(user.getId()).getEntity()).getUserName(), user.getUserName());
	}
	
	@Test
	public void testCreateNewUser() throws URISyntaxException, NoSuchAlgorithmException, UnsupportedEncodingException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		MockHttpRequest roleRequest = MockHttpRequest.get("/role?sSearch=Knight");
		Response response = roleResource.query(roleRequest.getUri());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Paged page = (Paged) response.getEntity();
		Role role = findFirstElement(page, Role.class);
		User user = new User();
		user.userName("Jorah")
			.firstName("Jorah")
			.lastName("Mormont")
			.password("sample")
			.role(role)
			.address("Bear Island");
		Response created = userResource.create(request.getUri(), null, user);
		assertEquals(HttpServletResponse.SC_CREATED, created.getStatus());
		Entry<String, List<Object>> location = created.getHeaders()
														.entrySet()
														.stream()
														.filter(predicate -> predicate.getKey().equals("Location"))
														.findAny()
														.get();
		assertTrue(location.getValue().stream().map(String::valueOf).anyMatch(value -> value.contains("/user/")));
		
		MockHttpRequest findJorah = MockHttpRequest.get("/user?sSearch=Jorah");
		Response userResponse = userResource.query(findJorah.getUri());
		assertEquals(HttpServletResponse.SC_OK, userResponse.getStatus());
		Paged userPage = (Paged) userResponse.getEntity();
		User jorah = findFirstElement(userPage, User.class);
		assertEquals(user.getRole(), jorah.getRole());
	}
	
	@Test
	public void testCreateUserWithNullRole() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		User user = new User().userName("Samwell")
							.firstName("Samwell")
							.lastName("Tarly")
							.password("sample")
							.address("Horn Hill");
		Response error = userResource.create(request.getUri(), null, user);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("invalid.role", ((Error) error.getEntity()).getLocalizedMessage());
	}
	
	@Test
	public void testCreateUserWithUnkownRole() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		User user = new User().userName("Samwell")
							.firstName("Samwell")
							.lastName("Tarly")
							.password("sample")
							.address("Horn Hill")
							.role(new Role().id(-1L));
		Response error = userResource.create(request.getUri(), null, user);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("invalid.role", ((Error) error.getEntity()).getLocalizedMessage());
	}
	
	@Test
	public void testCreateUserWithNullLoginName() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		User user = new User();
		Response error = userResource.create(request.getUri(), null, user);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("empty.invalid.loginName", ((Error) error.getEntity()).getLocalizedMessage());
	}
	
	@Test
	public void testCreateUserWithEmptyLoginName() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		User user = new User().userName("     ");
		Response error = userResource.create(request.getUri(), null, user);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("empty.invalid.loginName", ((Error) error.getEntity()).getLocalizedMessage());
	}
	
	@Test
	public void testCreateUserWithNullPassword() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user");
		User user = new User().userName("White Walker");
		Response error = userResource.create(request.getUri(), null, user);
		assertEquals(HttpServletResponse.SC_BAD_REQUEST, error.getStatus());
		assertEquals("empty.invalid.password", ((Error) error.getEntity()).getLocalizedMessage());
	}
	
	@Test
	public void testUpdateUser() throws URISyntaxException, NoSuchAlgorithmException, UnsupportedEncodingException {
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Jon");
		Response userResponse = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, userResponse.getStatus());
		Paged page = (Paged) userResponse.getEntity();
		User user = findFirstElement(page, User.class);
		
		MockHttpRequest roleRequest = MockHttpRequest.get("/role?sSearch=Lord%25");
		Response roleResponse = roleResource.query(roleRequest.getUri());
		assertEquals(HttpServletResponse.SC_OK, roleResponse.getStatus());
		Paged rolePage = (Paged) roleResponse.getEntity();
		Role role = findFirstElement(rolePage, Role.class);
		
		user.setRole(role);
		user.setPassword("Ygritte");
		Response response = userResource.update(user.getId(), null, user);
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		User userById = (User) userResource.get(user.getId()).getEntity();
		assertTrue(userById.getRole().getId().equals(role.getId()));
		assertNull(userById.getPassword());
	}
	
	@Test
	public void testDeleteUser() throws URISyntaxException {
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Bran");
		Response userResponse = userResource.query(request.getUri());
		assertEquals(HttpServletResponse.SC_OK, userResponse.getStatus());
		Paged page = (Paged) userResponse.getEntity();
		User user = findFirstElement(page, User.class);
		
		Response response = userResource.delete(user.getId());
		assertEquals(HttpServletResponse.SC_NO_CONTENT, response.getStatus());
		Error error = (Error) userResource.get(user.getId()).getEntity();
		assertEquals("RESOURCE_NOT_FOUND", error.getCode());
	}
	
	@Test
	public void testDeleteUserByName() throws URISyntaxException, NoSuchAlgorithmException, UnsupportedEncodingException {
		Response response = userResource.deleteByName("Ollena");
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		
		MockHttpRequest request = MockHttpRequest.get("/user?sSearch=Ollena");
		Paged user = (Paged) userResource.query(request.getUri()).getEntity();
		assertTrue(user.getITotalRecords().equals(0L));
	}
	
	private boolean isUserPresent(Paged page, String name) {
		return page.getAaData()
					.stream()
					.map(User.class::cast)
					.map(User::getFirstName)
					.anyMatch(name::contains);
	}
}
