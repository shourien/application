package com.myproject.app.rest.self;

import static org.junit.Assert.assertEquals;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import org.junit.Test;

import com.myproject.app.AbstractRestManager;
import com.myproject.app.rest.generated.models.Self;
import com.myproject.app.rest.self.SelfResource;

public class SelfResourceTest extends AbstractRestManager {
	
	private SelfResource selfResource;
	
	public SelfResourceTest() throws NamingException {
		selfResource = (SelfResource) context.lookup("java:global/app-rest/SelfResource");
	}
	
	@Test
	public void testSelfResourceName() {
		Response response = selfResource.get(securityInfo.getSecurityContext());
		assertEquals(HttpServletResponse.SC_OK, response.getStatus());
		Self self = (Self) response.getEntity();
		assertEquals("user", self.getName());
	}
	
}
