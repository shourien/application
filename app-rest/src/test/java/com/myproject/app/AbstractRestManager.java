package com.myproject.app;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.myproject.app.rest.generated.models.Paged;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.SecurityContext;
import javax.ejb.Stateless;
import javax.ejb.embeddable.EJBContainer;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractRestManager {

	protected static InitialContext context;
	
	protected static SecurityInfo securityInfo;
	
	@Stateless
	public static class SecurityInfo {
		
		@Context
		private SecurityContext securityContext;
		
		public SecurityContext getSecurityContext() {
			return this.securityContext;
		}
	}

	@BeforeClass
	public static void setup() throws DatabaseUnitException, SQLException, Exception {
		System.setProperty("h2.baseDir", "./target/");
		final Properties p = new Properties();
		p.load(AbstractRestManager.class.getClassLoader().getResourceAsStream("jndi.properties"));
		EJBContainer.createEJBContainer(p);
		context = new InitialContext(p);
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet(AbstractRestManager.class.getSimpleName()));
		securityInfo = (SecurityInfo) context.lookup("java:global/app-rest/SecurityInfo");
	}

	@AfterClass
	public static void tearDown() throws NamingException {
		if (context != null) {
			context.close();
		}
	}
	
	protected <T> T findFirstElement(Paged page, Class<T> clazz) {
		return page.getAaData()
					.stream()
					.map(clazz::cast)
					.findFirst()
					.get();
	}
	
	private static IDataSet getDataSet(String resourceName) throws Exception {
        InputStream inputStream = AbstractRestManager.class.getClassLoader().getResourceAsStream(resourceName + ".xml");
        return new FlatXmlDataSetBuilder().build(inputStream);
    }
	
	private static IDatabaseConnection getConnection() throws Exception {
		DataSource dataSource = (DataSource) context.lookup("java:openejb/Resource/myProjDatabase");
		return new DatabaseConnection(dataSource.getConnection());
	}
}
