package com.myproject.app.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@MappedSuperclass
public abstract class PersistentObject implements Serializable, Identifiable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 2628750049711291040L;
	/**
	 * This column holds the last updated time of a row on each table.
	 * <p/>
	 * The purpose of this column is to hold the last updated time of any row
	 * and also use this info for the database replication.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "UPDATED", nullable = true)
	private Date updated;

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	/**
	 * This column holds the inserted time of a row on each table.
	 * <p/>
	 * The purpose of this column is to hold the inserted time of any row and
	 * also use this info for the database replication.
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "INSERTED", nullable = true)
	private Date inserted;

	public void setInserted(Date inserted) {
		this.inserted = inserted;
	}

	public Date getInserted() {
		return inserted;
	}

	@PrePersist
	@PreUpdate
	protected void setTime() {
		updated = new Date();

		// only set the inserted for the new rows.
		if (inserted == null)
			inserted = new Date();
	}
}