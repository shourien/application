package com.myproject.app.entities;

public interface Identifiable {
	Long getId();
}
