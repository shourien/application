package com.myproject.app.db;

import javax.persistence.Query;

public class PageInfo {
	public final Integer start;
    public final Integer maxCount;

    public PageInfo(Integer start, Integer maxCount) {
        this.start = start;
        this.maxCount = maxCount;
    }

    public static <Q extends Query > Q applyPaging(Q query, PageInfo pageInfo) {
        if (pageInfo != null) {
            if (pageInfo.start != null) {
                query.setFirstResult(pageInfo.start);
            }
            if (pageInfo.maxCount != null && pageInfo.maxCount != -1) {
                query.setMaxResults(pageInfo.maxCount);
            }
        }
        return query;
    }
}
