package com.myproject.app.db;

import java.util.List;
import java.util.Optional;

public interface BaseService {
    <T> T save(T entity);

    <T> T updateObject(T entity);

    void deleteObject(Object entity);

    <T> Optional<T> findById(Class<T> clazz, Long id);

    <T> List<T> findAll(Class<T> clazz);
}

