package com.myproject.app.db;

import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

/**
 * Just querying
 */
public interface GenericQueryHandler<T> {

    List<T> findAll(PageInfo pageInfo);

    /**
     * @return entity or null if not exists
     */
    Optional<T> findById(Long id);

    Optional<Long> countAll(String name);

    Optional<T> findByName(String name);

    /**
     * @return all entities that match the name expression. Query wildcards (%, ?) can be used
     */
    List<T> findByName(String nameExpression, PageInfo pageInfo);

    default List<T> getQueryResult(PageInfo pageInfo, TypedQuery<T> query) {
        return PageInfo.applyPaging(query, pageInfo).getResultList();
    }
}
