package com.myproject.app.db;

import com.myproject.app.exception.DatabaseException;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

/**
 * Responsible for validation etc..
 */
public interface GenericPersistenceHandler<T, SnapshotType> {

    Long persist(T instance) throws DatabaseException;
    
    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    void delete(T object);

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    Long updated(T type) throws DatabaseException;

    SnapshotType snapshot(T type);
    
}
