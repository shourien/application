package com.myproject.app.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

public class BaseServiceImpl implements BaseService {

    private static final Logger log = LoggerFactory.getLogger(BaseServiceImpl.class);
    
    @PersistenceContext(unitName = "myproj-app")
    protected EntityManager entityManager;

	public <T> T save(T entity) {
		try {
            entityManager.persist(entity);
        } catch (PersistenceException e) {
            log.error("Exception occured while trying to save object of type "+entity.getClass().getName(), e);
            throw e;
        }
        return entity;
	}

	public <T> T updateObject(T entity) {
		return entityManager.merge(entity);
	}

	public void deleteObject(Object entity) {
		entityManager.remove(entity);
	}

	public <T> Optional<T> findById(Class<T> clazz, Long id) {
		T result = entityManager.find(clazz, id);
        if (result != null) {
        	return Optional.of(result);
        }
        return Optional.empty();
	}

	@Override
	public <T> List<T> findAll(Class<T> clazz) {
		List<T> results = entityManager.createQuery("from " + clazz.getName()).getResultList();
		return Optional.ofNullable(results).orElse(Collections.emptyList());
	}
}
