package com.myproject.app.db;

import java.util.List;
import java.util.Optional;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.myproject.app.entities.PersistentObject;
import com.myproject.app.exception.DatabaseException;

public class HibernateDao<T extends PersistentObject, SnapshotType> extends BaseServiceImpl implements GenericPersistenceHandler<T, SnapshotType>, GenericQueryHandler<T> {

	private final Class<T> type;
    private final String nameProperty;
    protected static final Logger log = LoggerFactory.getLogger(HibernateDao.class);
    
    public HibernateDao(Class<T> type, String nameProperty) {
        this.type = type;
        this.nameProperty = nameProperty;
    }
    
	@Override
	public List<T> findAll(PageInfo pageInfo) {
		CriteriaQueryContext context = new CriteriaQueryContext(entityManager);
        TypedQuery<T> query = entityManager.createQuery(context.criteriaQuery);
        return getQueryResult(pageInfo, query);
	}

	@Override
	public Optional<T> findById(Long id) {
		return findById(type, id);
	}

	@Override
	public Optional<Long> countAll(String name) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
        Root<T> root = criteriaQuery.from(type);
        criteriaQuery.select(criteriaBuilder.count(root));
        if(name != null) {
        	applyWhereClause(criteriaQuery, criteriaBuilder.like(root.get(nameProperty), name));
        } else {
        	applyWhereClause(criteriaQuery, null);
        }
        TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
        return Optional.of(query.getSingleResult());
	}

	@Override
	public Optional<T> findByName(String name) {
		CriteriaQueryContext context = new CriteriaQueryContext(entityManager);
		applyWhereClause(context.criteriaQuery, context.criteriaBuilder.equal(context.root.get(nameProperty), name));
		TypedQuery<T> query = entityManager.createQuery(context.criteriaQuery);
        try {
            return Optional.of(query.getSingleResult());
        } catch (NoResultException e) {
            return Optional.empty();
        }
	}

	@Override
	public List<T> findByName(String nameExpression, PageInfo pageInfo) {
		CriteriaQueryContext context = new CriteriaQueryContext(entityManager);
		applyWhereClause(context.criteriaQuery, context.criteriaBuilder.like(context.root.get(nameProperty), nameExpression));
		TypedQuery<T> query = entityManager.createQuery(context.criteriaQuery);
        return getQueryResult(pageInfo, query);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public Long persist(T instance) throws DatabaseException {
		T save = save(instance);
		return save.getId();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public void delete(T object) {
		deleteObject(object);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.MANDATORY)
	public Long updated(T object) throws DatabaseException {
		T update = updateObject(object);
		return update.getId();
	}

	@Override
	public SnapshotType snapshot(T type) {
		return null;
	}
	
	private class CriteriaQueryContext {
		public final CriteriaBuilder criteriaBuilder;
        public final CriteriaQuery<T> criteriaQuery;
        public final Root<T> root;

        public CriteriaQueryContext(EntityManager entityManager) {
            criteriaBuilder = entityManager.getCriteriaBuilder();
            criteriaQuery = criteriaBuilder.createQuery(type);
            root = criteriaQuery.from(type);
            criteriaQuery.select(root);
        }
	}
	
	private void applyWhereClause(CriteriaQuery<?> criteriaQuery, Predicate userPredicate){
        if(userPredicate != null) {
            criteriaQuery.where(userPredicate);
        }
    }

}
