package com.myproject.app.entities.user;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.List;

import com.myproject.app.AbstractJPARepository;
import com.myproject.app.entities.RoleEntity;
import com.myproject.app.entities.UserEntity;

public class UserEntityTest extends AbstractJPARepository {

	public UserEntityTest() throws Exception {
		super(UserEntityTest.class);
	}

	@Test
	public void testPersistUser() throws Exception {
		UserEntity user = new UserEntity();
		user.setLogin("User");
		user.setFirstName("Shourien");
		UserEntity id = baseService.save(user);
		assertNotNull(id);
	}
	
	@Test
	public void testFindUser() throws Exception {
		List<UserEntity> users = baseService.findAll(UserEntity.class);
		assertTrue(users.stream()
						.map(UserEntity::getLogin)
						.filter("Random"::equals)
						.findFirst()
						.isPresent());
	}
	
	@Test
	public void testDeleteRole() throws Exception {
		List<UserEntity> allUsers = baseService.findAll(UserEntity.class);
		baseService.deleteObject(allUsers.stream().findFirst().get());
		List<UserEntity> users = baseService.findAll(UserEntity.class);
		assertSame(allUsers.size() - 1, users.size());
	}
	
	@Test
	public void testUpdateRole() throws Exception {
		UserEntity entity = baseService.findAll(UserEntity.class)
									.stream()
									.filter(predicate -> predicate.getLogin().equals("Random"))
									.findFirst()
									.get();
		
		entity.setPrimAddress("Melbourne");
		assertTrue(baseService.findAll(UserEntity.class)
							.stream()
							.map(UserEntity::getPrimAddress)
							.filter("Melbourne"::equals)
							.findFirst()
							.isPresent());
	}
	
	@Test
	public void testRoleAssociationWithUser() throws Exception {
		UserEntity user = baseService.findAll(UserEntity.class)
									.stream()
									.filter(predicate -> predicate.getLogin().equals("Random"))
									.findFirst()
									.get();
		assertNull(user.getRole());
		RoleEntity role = baseService.findAll(RoleEntity.class)
						.stream()
						.filter(predicate -> predicate.getRoleName().equals("Engineering"))
						.findFirst()
						.get();
		user.setRole(role);
		baseService.save(user);
		
		assertTrue(baseService.findAll(UserEntity.class)
							.stream()
							.filter(predicate -> predicate.getLogin().equals("Random"))
							.map(UserEntity::getRole)
							.map(RoleEntity::getRoleName)
							.anyMatch("Engineering"::equals));
	}
}
