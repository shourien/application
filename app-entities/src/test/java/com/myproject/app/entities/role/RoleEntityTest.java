package com.myproject.app.entities.role;

import com.myproject.app.AbstractJPARepository;
import com.myproject.app.entities.RoleEntity;
import com.myproject.app.entities.UserEntity;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import static org.junit.Assert.*;

public class RoleEntityTest extends AbstractJPARepository {

	public RoleEntityTest() throws Exception {
		super(RoleEntityTest.class);
	}

	@Test
	public void testPersistRole() throws Exception {
		RoleEntity role = new RoleEntity();
		role.setRoleName("Engineer");
		RoleEntity id = baseService.save(role);
		assertNotNull(id);
	}
	
	@Test
	public void testFetchRole() throws Exception {
		List<RoleEntity> roles = baseService.findAll(RoleEntity.class);
		assertTrue(roles.stream()
						.map(RoleEntity::getRoleName)
						.filter("Testing"::equals)
						.findFirst()
						.isPresent());
	}
	
	@Test
	public void testDeleteRole() throws Exception {
		List<RoleEntity> allRoles = baseService.findAll(RoleEntity.class);
		baseService.deleteObject(allRoles.stream().findFirst().get());
		List<RoleEntity> roles = baseService.findAll(RoleEntity.class);
		assertSame(allRoles.size() - 1, roles.size());
	}
	
	@Test
	public void testUpdateRole() throws Exception {
		RoleEntity entity = baseService.findAll(RoleEntity.class)
									.stream()
									.filter(predicate -> predicate.getRoleName().equals("Testing"))
									.findFirst()
									.get();
		entity.setRoleName("Engineering");
		assertTrue(baseService.findAll(RoleEntity.class)
				.stream()
				.map(RoleEntity::getRoleName)
				.filter("Engineering"::equals)
				.findFirst()
				.isPresent());
	}
	
	@Test
	public void testUserAssociationWithRole() throws Exception {
		UserEntity user = baseService.findAll(UserEntity.class)
									.stream()
									.filter(predicate -> predicate.getLogin().equals("User"))
									.findFirst()
									.get();
		RoleEntity role = baseService.findAll(RoleEntity.class)
									.stream()
									.filter(predicate -> predicate.getRoleName().equals("Testing"))
									.findFirst()
									.get();
		user.setRole(role);
		baseService.save(user);
		Set<UserEntity> users = baseService.findAll(RoleEntity.class)
										.stream()
										.filter(predicate -> predicate.getRoleName().equals("Testing"))
										.map(RoleEntity::getUsers)
										.flatMap(Set::stream)
										.collect(Collectors.toSet());
		
		assertSame(1, users.size());
		assertTrue(users.stream()
						.map(UserEntity::getRole)
						.map(RoleEntity::getId)
						.anyMatch(role.getId()::equals));
	}
}