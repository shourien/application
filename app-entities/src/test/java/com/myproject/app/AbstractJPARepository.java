package com.myproject.app;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.myproject.app.db.BaseService;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ejb.embeddable.EJBContainer;

import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Optional;
import java.util.Properties;

public abstract class AbstractJPARepository {

	protected static InitialContext context;
	
	protected static BaseService baseService;
	
	public <T extends AbstractJPARepository> AbstractJPARepository(Class<T> clazz) throws Exception {
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet(AbstractJPARepository.class.getSimpleName()).get());
		Optional<IDataSet> dataSet = getDataSet(clazz.getSimpleName());
		if (dataSet.isPresent()) {
			DatabaseOperation.INSERT.execute(getConnection(), dataSet.get());
		}
	}

	@BeforeClass
	public static void setup() throws NamingException, IOException {
		System.setProperty("h2.baseDir", "./target/");
		final Properties p = new Properties();
		p.load(AbstractJPARepository.class.getClassLoader().getResourceAsStream("jndi.properties"));
		EJBContainer.createEJBContainer(p);
		context = new InitialContext(p);
		baseService = (BaseService) context.lookup("java:global/app-entities/BaseServiceImpl");
	}

	@AfterClass
	public static void tearDown() throws NamingException {
		if (context != null) {
			context.close();
		}
	}
	
	private Optional<IDataSet> getDataSet(String resourceName) throws DataSetException {
        InputStream inputStream = AbstractJPARepository.class.getClassLoader().getResourceAsStream(resourceName + ".xml");
        return inputStream != null ? Optional.of(new FlatXmlDataSetBuilder().build(inputStream)) : Optional.empty();
    }
	
	private IDatabaseConnection getConnection() throws NamingException, DatabaseUnitException, SQLException {
		DataSource dataSource = (DataSource) context.lookup("java:openejb/Resource/myProjDatabase");
		return new DatabaseConnection(dataSource.getConnection());
	}
}
