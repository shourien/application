package com.myproject.app.management.user;

import com.myproject.app.db.HibernateDao;
import com.myproject.app.entities.UserEntity;

import javax.ejb.Stateless;

@Stateless
public class UserDaoImpl extends HibernateDao<UserEntity, Long> implements UserDao {
	
	public UserDaoImpl() {
        super(UserEntity.class, "login");
    }
	
}
