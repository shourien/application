package com.myproject.app.management.role;

import javax.ejb.Local;

import com.myproject.app.db.GenericPersistenceHandler;
import com.myproject.app.db.GenericQueryHandler;
import com.myproject.app.entities.RoleEntity;

@Local
public interface RoleDao extends GenericPersistenceHandler<RoleEntity, Long>, GenericQueryHandler<RoleEntity> {
}
