package com.myproject.app.management.user;

import javax.ejb.Local;

import com.myproject.app.db.GenericPersistenceHandler;
import com.myproject.app.db.GenericQueryHandler;
import com.myproject.app.entities.UserEntity;

@Local
public interface UserDao extends GenericPersistenceHandler<UserEntity, Long>, GenericQueryHandler<UserEntity> {
}
