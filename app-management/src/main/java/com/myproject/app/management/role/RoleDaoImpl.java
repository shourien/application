package com.myproject.app.management.role;

import javax.ejb.Stateless;

import com.myproject.app.db.HibernateDao;
import com.myproject.app.entities.RoleEntity;

@Stateless
public class RoleDaoImpl extends HibernateDao<RoleEntity, Long> implements RoleDao {

	public RoleDaoImpl() {
		super(RoleEntity.class, "roleName");
	}
}
