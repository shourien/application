package com.myproject.app.management.user;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;

import javax.ejb.EJBTransactionRequiredException;
import javax.naming.NamingException;

import org.junit.Test;

import com.myproject.app.AbstractDaoManager;
import com.myproject.app.db.PageInfo;
import com.myproject.app.entities.RoleEntity;
import com.myproject.app.entities.UserEntity;
import com.myproject.app.exception.DatabaseException;
import com.myproject.app.management.role.RoleDao;

public class UserDaoTest extends AbstractDaoManager {

	private UserDao userDao;
	
	private RoleDao roleDao;
	
	public UserDaoTest() throws NamingException {
		userDao = (UserDao) context.lookup("java:global/app-management/UserDaoImpl");
		roleDao = (RoleDao) context.lookup("java:global/app-management/RoleDaoImpl");
	}
	
	@Test
	public void findExistingUser() {
		Optional<UserEntity> user = userDao.findByName("Hand");
		assertEquals(user.get().getFirstName(), "Tywin");
	}
	
	@Test
	public void findAllExistingUsers() {
		List<UserEntity> users = userDao.findAll(null);
		assertTrue(users.size() > 1);
		assertTrue(users.stream()
						.filter(predicate -> predicate.getLogin().equals("Random"))
						.findAny()
						.isPresent());
	}
	
	@Test
	public void findExistingUsersPageWise() {
		PageInfo page = new PageInfo(0, 1);
		assertTrue(userDao.findAll(page).size() == 1);
	}
	
	@Test
	public void findUsersByNameExpression() {
		List<UserEntity> users = userDao.findByName("Mount%", null);
		assertEquals(1, users.size());
		assertTrue(users.stream()
						.map(UserEntity::getLogin)
						.filter("Mountain"::equals)
						.findAny()
						.isPresent());
	}
	
	@Test
	public void findExistingUserById() {
		Optional<UserEntity> user = userDao.findByName("Hand");
		Optional<UserEntity> userId = userDao.findById(user.get().getId());
		assertEquals(user.get().getId(), userId.get().getId());
	}
	
	@Test
	public void testPersistUser() throws Exception {
		UserEntity user = new UserEntity();
		user.setFirstName("First");
		user.setLastName("Last");
		user.setLogin("Login");
		user.setPrimAddress("Brisbane");
		RoleEntity role = roleDao.findByName("Testing").get();
		user.setRole(role);
		Long userId = transactionalCaller.call(() -> userDao.persist(user));
		assertNotNull(userId);
	}
	
	@Test(expected = EJBTransactionRequiredException.class)
	public void testPersistUserWithoutTransaction() throws DatabaseException {
		UserEntity user = new UserEntity();
		user.setFirstName("First");
		user.setLastName("Last");
		user.setLogin("Login");
		user.setPrimAddress("Brisbane");
		userDao.persist(user);
	}
	
	@Test
	public void testUpdateUser() throws Exception {
		UserEntity user = userDao.findByName("Random").get();
		assertNull(user.getRole());
		RoleEntity role = roleDao.findByName("Testing").get();
		user.setRole(role);
		transactionalCaller.call(() -> {
        	Long userId = userDao.updated(user);
        	assertEquals("Testing", userDao.findById(userId).get().getRole().getRoleName());
        	return userId;
		});
	}
	
	@Test(expected = EJBTransactionRequiredException.class)
	public void testUpdateUserWithoutTransaction() throws DatabaseException {
		UserEntity user = userDao.findByName("Mountain").get();
		assertNull(user.getRole());
		RoleEntity role = roleDao.findByName("Testing").get();
		user.setRole(role);
		userDao.updated(user);
	}
	
	@Test
	public void testDeleteUser() throws Exception {
		transactionalCaller.call(() -> {
        	userDao.delete(userDao.findByName("User").get());
			return null;
		});
		assertFalse(userDao.findByName("User").isPresent());
	}
	
	@Test(expected = EJBTransactionRequiredException.class)
	public void testDeleteUserWithoutTransaction() throws Exception {
		userDao.delete(userDao.findByName("Mountain").get());
	}
}