package com.myproject.app.management.role;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Optional;
import javax.ejb.EJBTransactionRequiredException;
import javax.naming.NamingException;

import org.junit.Test;

import com.myproject.app.AbstractDaoManager;
import com.myproject.app.db.PageInfo;
import com.myproject.app.entities.RoleEntity;
import com.myproject.app.exception.DatabaseException;

public class RoleDaoTest extends AbstractDaoManager {
	
	private RoleDao roleDao;
	
	public RoleDaoTest() throws NamingException {
		roleDao = (RoleDao) context.lookup("java:global/app-management/RoleDaoImpl");
	}
	
	@Test
	public void findExistingRole() {
		Optional<RoleEntity> role = roleDao.findByName("Testing");
		assertNotNull(role.get().getId());
	}
	
	@Test
	public void findAllExistingRoles() {
		List<RoleEntity> roles = roleDao.findAll(null);
		assertTrue(roles.size() > 1);
		assertTrue(roles.stream()
						.filter(predicate -> predicate.getRoleName().equals("Testing"))
						.findAny()
						.isPresent());
	}
	
	@Test
	public void findExistingRolesPageWise() {
		PageInfo page = new PageInfo(0, 1);
		assertEquals(roleDao.findAll(page).size(), 1);
	}
	
	@Test
	public void findRolesByNameExpression() {
		List<RoleEntity> roles = roleDao.findByName("Test%", null);
		assertEquals(1, roles.size());
		assertTrue(roles.stream()
						.map(RoleEntity::getRoleName)
						.filter("Testing"::equals)
						.findAny()
						.isPresent());
	}
	
	@Test
	public void findExistingRoleById() {
		Optional<RoleEntity> role = roleDao.findByName("Testing");
		Optional<RoleEntity> roleId = roleDao.findById(role.get().getId());
		assertEquals(role.get().getId(), roleId.get().getId());
	}
	
	@Test
	public void testPersistRole() throws Exception {
		RoleEntity role = new RoleEntity();
		role.setRoleName("Services");
		Long roleId = transactionalCaller.call(() -> roleDao.persist(role));
		assertNotNull(roleId);
	}
	
	@Test(expected = EJBTransactionRequiredException.class)
	public void testPersistRoleWithoutTransaction() throws DatabaseException {
		RoleEntity role = new RoleEntity();
		role.setRoleName("Services");
		roleDao.persist(role);
	}
	
	@Test
	public void testUpdateRole() throws Exception {
		RoleEntity role = roleDao.findByName("Engineering").get();
		role.setRoleName("Services");
		transactionalCaller.call(() -> {
        	Long roleId = roleDao.updated(role);
        	assertEquals("Services", roleDao.findById(roleId).get().getRoleName());
        	return roleId;
		});
	}
	
	@Test(expected = EJBTransactionRequiredException.class)
	public void testUpdateRoleWithoutTransaction() throws DatabaseException {
		RoleEntity role = roleDao.findByName("IT Admin").get();
		role.setRoleName("Administration");
		roleDao.updated(role);
	}
	
	@Test
	public void testDeleteUser() throws Exception {
		transactionalCaller.call(() -> {
        	roleDao.delete(roleDao.findByName("Documentation").get());
			return null;
		});
		assertFalse(roleDao.findByName("Documentation").isPresent());
	}
	
	@Test(expected = EJBTransactionRequiredException.class)
	public void testDeleteRoleWithoutTransaction() throws Exception {
		roleDao.delete(roleDao.findByName("IT Admin").get());
	}
}
