package com.myproject.app;

import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.operation.DatabaseOperation;
import org.junit.AfterClass;
import org.junit.BeforeClass;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.embeddable.EJBContainer;

import static javax.ejb.TransactionAttributeType.REQUIRES_NEW;

import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.Callable;

public abstract class AbstractDaoManager {

	protected static InitialContext context;
	
	protected static Caller transactionalCaller;
	
	public static interface Caller {
        public <T> T call(Callable<T> callable) throws Exception;
    }
	
	@Stateless
    @TransactionAttribute(REQUIRES_NEW)
    public static class TransactionBean implements Caller {

        public <T> T call(Callable<T> callable) throws Exception {
            return callable.call();
        }
    }

	@BeforeClass
	public static void setup() throws DatabaseUnitException, SQLException, Exception {
		System.setProperty("h2.baseDir", "./target/");
		final Properties p = new Properties();
		p.load(AbstractDaoManager.class.getClassLoader().getResourceAsStream("jndi.properties"));
		EJBContainer.createEJBContainer(p);
		context = new InitialContext(p);
		transactionalCaller = (Caller) context.lookup("java:global/app-management/TransactionBean");
		DatabaseOperation.CLEAN_INSERT.execute(getConnection(), getDataSet(AbstractDaoManager.class.getSimpleName()));
	}

	@AfterClass
	public static void tearDown() throws NamingException {
		if (context != null) {
			context.close();
		}
	}
	
	private static IDataSet getDataSet(String resourceName) throws Exception {
        InputStream inputStream = AbstractDaoManager.class.getClassLoader().getResourceAsStream(resourceName + ".xml");
        return new FlatXmlDataSetBuilder().build(inputStream);
    }
	
	private static IDatabaseConnection getConnection() throws Exception {
		DataSource dataSource = (DataSource) context.lookup("java:openejb/Resource/myProjDatabase");
		return new DatabaseConnection(dataSource.getConnection());
	}
}
