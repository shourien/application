# Application Project 
![JAVA](http://i.imgur.com/2TgGt9L.png "JAVA 8") ![EJB3](http://i.imgur.com/7C30Ifq.png "EJB 3.0") ![SWAGGER](https://app.swaggerhub.com/img/favicon-32x32.png "Swagger 2.0") ![HIBERNATE](http://i.imgur.com/bvvfkOY.png "Hibernate 5") ![JPA](http://i.imgur.com/73KeY8Z.png "JPA 2.0") ![JAAS](http://i.imgur.com/ge8PSaj.png "JAAS Security") ![JUNIT](https://s3-ap-northeast-1.amazonaws.com/qiita-tag-image/bede974ba48a9bc1b123aac2ac22356ccf78da0e/medium.jpg?1450590252 "Junit4/Mockito") ![PYTHON](https://s3-ap-northeast-1.amazonaws.com/qiita-tag-image/28fd3d6b220c89e6197fd82c02fd2fcd2bb66d81/medium.jpg?1383884245 "Python 2.7") ![H2](http://i.imgur.com/d1opEOl.png "H2 Database")  ![Wildfly](http://i.imgur.com/Zahm4Am.gif "Wildfly 10") ![MAVEN](http://i.imgur.com/DnUDQZs.png "Maven 3") ![ANT](http://i.imgur.com/FacmOB0.png "Maven Ant Plugin") ![DOCKER](https://i.imgur.com/z5VbXxp.jpg "DOCKER") ![AWS](https://i.imgur.com/qzFEQPU.png "Amazon Webservices EC2") 

#[ >>>DEMO LINK<<< ![CLICK](http://sod-a.rsc-cdn.org/www.rsc.org/learn-chemistry/content/Images/icon_new_window.png)](http://18.216.86.231/ui)
  This project is hosted using the AWS account I own on a single node cluster.

* *username:* __super__ 
* *password:* __super__

This is a sample project that demonstrates an end to end boilerplate code for how a web application can be developed. 
The application is complete with management of 2 basic attributes in general associated with an application `User` and `Role`. 
This is an EJB3 based rest resource exposed behind an authorization scheme of Basic. The Rest resource is exposed using swagger 2.0 UI where `User` management as well as `Role` management(Create, Read, Update, Delete) can be performed. 
Some of the technologies used with this application are more suited to give a general understanding as to having a scalable 
and robust architecture as well as help easy development and quick prototyping in an application.

## Application Console [WIKI ![CLICK](http://sod-a.rsc-cdn.org/www.rsc.org/learn-chemistry/content/Images/icon_new_window.png)](https://bitbucket.org/shourien/application/wiki/Application%20Console%20with%20AngularJS)
![AngularJS](https://i.imgur.com/l4aJdyj.jpg "AngularJS") ![Bootstrap3](https://i.imgur.com/rocFt1k.png "Bootstrap3") ![Gulp](https://i.imgur.com/sY1Vh4F.png "Gulp") ![Node](https://i.imgur.com/lrxCSOk.png "NodeJs") ![Angular UI Bootstrap](https://i.imgur.com/WJ7SEwE.png "Angular Bootstrap") ![HighCharts](https://i.imgur.com/biFL9th.png "HighCharts") ![NPM](https://i.imgur.com/EhER4Ss.png "NPM") ![Dragular API](https://i.imgur.com/mGVXliu.png "Dragular API")

The AngularJS GUI console application using AngularJS 1.4 and Bootstrap 3 CSS. This is hosted by default on the URL 
(http://localhost:8090/ui)

### Architecture Diagram

![Architecture Diagram](https://bitbucket.org/shourien/application/downloads/Architecture_Diagram.png)

On this page:

* [Getting Started - Docker](https://bitbucket.org/shourien/application/overview#markdown-header-getting-started-docker)
    * [Prerequisites Docker](https://bitbucket.org/shourien/application/overview#markdown-header-prerequisites-docker)

    * [Installing With Docker](https://bitbucket.org/shourien/application/overview#markdown-header-installing-with-docker)
    
* [Getting Started - Classic Installer](https://bitbucket.org/shourien/application/overview#markdown-header-getting-started-classic-installer)
    * [Prerequisites Classic Installer](https://bitbucket.org/shourien/application/overview#markdown-header-prerequisites-classic-installer)

    * [Installing Classic Installer](https://bitbucket.org/shourien/application/overview#markdown-header-installing-classic-installer)

* [Deployment](https://bitbucket.org/shourien/application/overview#markdown-header-deployment)
    * [Start Server](https://bitbucket.org/shourien/application/overview#markdown-header-starting-the-application)

    * [Stop Server](https://bitbucket.org/shourien/application/overview#markdown-header-stopping-the-application)

    * [Other Options](https://bitbucket.org/shourien/application/overview#markdown-header-lists)

* [Usage](https://bitbucket.org/shourien/application/overview#markdown-header-usage)

* [Application Walkthrough](https://bitbucket.org/shourien/application/overview#markdown-header-application-walkthrough)

    * [Login Page](https://bitbucket.org/shourien/application/overview#markdown-header-login-page)
    
    * [Welcome screen](https://bitbucket.org/shourien/application/overview#markdown-header-welcome-screen)
    
    * [User REST service](https://bitbucket.org/shourien/application/overview#markdown-header-user-rest-service)

* [Technology Stack](https://bitbucket.org/shourien/application/overview#markdown-header-technology-stack-used-for-this-project)

    * [Swagger 2.0 UI](https://bitbucket.org/shourien/application/overview#markdown-header-swagger2-ui)
    
    * [JAAS](https://bitbucket.org/shourien/application/overview#markdown-header-jaas)
    
* [Running Tests](https://bitbucket.org/shourien/application/overview#markdown-header-running-the-tests)

    * [Breakdown into end-end tests](https://bitbucket.org/shourien/application/overview#markdown-header-break-down-into-end-to-end-tests)
    
    * [And coding style tests](https://bitbucket.org/shourien/application/overview#markdown-header-and-coding-style-tests)
    
* [Building With](https://bitbucket.org/shourien/application/overview#markdown-header-building-with)

    * [Generate Installer File](https://bitbucket.org/shourien/application/overview#markdown-header-installer-zip-files)
    
    

* [Versioning](https://bitbucket.org/shourien/application/overview#markdown-header-versioning)

* [Authors](https://bitbucket.org/shourien/application/overview#markdown-header-authors)

* [License](https://bitbucket.org/shourien/application/overview#markdown-header-license)

* [Improvements](https://bitbucket.org/shourien/application/overview#markdown-header-improvements)

##Getting Started Docker [WIKI ![CLICK](http://sod-a.rsc-cdn.org/www.rsc.org/learn-chemistry/content/Images/icon_new_window.png)](https://bitbucket.org/shourien/application/wiki/Application%20Docker%20Install%20and%20AWS)

The Docker installation is the easiest way to install the application. The application's docker installer is uploaded to the [Docker cloud](https://hub.docker.com/r/shouriendoc/application/) 
and would be publicly available automatically for docker to be installed on the target machine. However, the only prerequisite is to install the docker software. Unlike VMs, Docker containers do not bundle a full operating system - only libraries and settings required to make the software work without any manual configurations.

  1) Install Docker
  

### Prerequisites Docker

Docker installer files are available for Windows, Mac, and Linux operating systems. 

  * Docker [Docker](https://www.docker.com/)

### Installing with Docker

To install the application with docker all that needs to be executed is the following command
```
docker run -e app_jdbc_url='jdbc:h2:mem:project' -e app_jdbc_username='sa' -e app_jdbc_password='sa'  -p 8090:8080 shouriendoc/application:0.0.1-SNAPSHOT
```
Executing this single command will automatically pull the docker container from the cloud repository and set up the server listening on port 8090.
The application defaults with the in-memory JDBC URL `jdbc:h2:mem:project` username and password of `sa` which of course can be changed based 
on user.

## Getting Started Classic Installer

The project can be downloaded as an installer directly from the following [Download Link](http://appdemos.org/application/app-installer-0.0.1-SNAPSHOT.zip). 
Before you can install the package you would need to have 2 of the prerequisites met

 1) Install JDK 8 
 
 2) Install Python 2.7

#### Application Glimpse

Below is the GUI Login Screen that shows up when you login into the application

![Welcome Screen](https://i.imgur.com/hucUWVH.png)

### Prerequisites Classic Installer

For this project to work following are the tools that need to be installed in the system.
The advantage of choosing these 3 technologies is that they are platform independent. Having shell scripts for installation 
and deployment can lead to frustrating deployments across platforms.

* Java 8 

* Python 2.7 For installation and deployment scripts [Phyton 2.7](https://www.python.org/download/releases/2.7/)

* __*Optional*__  Maven 3.5 For building the project and dependency management [Maven 3.5](http://apache.mirror.digitalpacific.com.au/maven/maven-3/3.5.0/binaries/)


### Installing Classic Installer

Installation of the application is supported on any platform be it Windows, Linux as well as UNIX.

For Installation of the application, follow the following steps

 1) unzip app-installer-0.0.1-SNAPSHOT.zip
This will extract 3 files within the zip archive
app-build.zip, app.cfg and install.py

 2) Configure the app.cfg to provide 3 parameters in particular
  app.install.dir=/opt/app
  
  java.jdk.home=/opt/jdk1.8.0_144
  
  and
  
  app.jdbc.connection.url=jdbc:h2:/opt/app/project

  Rest of the parameters can be left untouched which would mean application will start on port 8090 by default 
  on localhost with an H2 database created in /opt/app folder having the credentials as `sa` username and `sa` password
  
3) Execute the install.py script to start the installation in the desired folder provided in the app.cfg earlier.
 
```
$install.py
APPLICATION installer. Use --help for options

         Installing at /opt/app

         Creating new installation directory /opt/app ...... done

         Installing 'JBoss' to /opt/app ...... done

         Generating app.conf at /opt/app/wildfly-10.1.0.Final/bin ...... done

         Configuring standalone.xml with datasource ...... done

#################################################################################
#                      Installation Completed Successfully                      #
#################################################################################

```

## Deployment

The application is deployed by starting the JBoss Wildfly server 10. However, the application can be started with a 
simple step without worrying about the target machine being Windows or Linux with the following ways

### Starting the Application

To start the application you would need to navigate to the bin directory of Wildfly and start the server as follows
```
    cd /opt/app/wildfly-10.1.0.Final/bin
    $ app.py start
    Waiting for server to start . . . . . .
    Server successfully started in 124 seconds
```

### Stopping the Application

To stop the application you would need to execute the command
```
    $ app.py stop
    Running process .. 5420
    {"outcome" => "success"}
    Waiting for server to shutdown . . Server successfully shutdown

    No processes are running
```
### Other Options

Help on various deployment options while working with application
```
    Usage: app.py
    start : Starts APP Server
    stop : Stops APP Server
    restart : Restarts APP Server
    status : Displays current status of server (started/stopped)
```

## Usage

The application by default is being hosted on the URL http://localhost:8090/rest/doc/index.html

Upon hitting this URL the Basic authentication kicks in and user can enter the default username and password to log in
* *username:* __super__ 
* *password:* __super__

This will provide access to the Swagger UI for accessing the REST API's.


## Application Walkthrough

Once the application is installed and started the user can navigate to the welcome screen by entering the URL credentials 
http://localhost:8090/rest/doc/index.html by default. The port is actually determined by the parameter in the app.cfg as jboss.socket.binding.port-offset which offsets the port from the default 8080 port at which the Wildly server usually starts. 
Currently OOTB the offset is set to the value 10 therefore, The server starts with 8080 + 10 at the port 8090 by default.

### Login Page

Basic authorization is used with the use of JAAS (Java Authentication & Authorization Service) to authenticate the user. Once JBoss
authenticates the user by looking up in either the users.properties file present in configuration folder or the database with the `principalQuery` being provided in the standalone xml the welcome screen is presented on successful authentication. 
By default **super** (username) and **super** (password) user is added OOTB to the system to login.

![Login Screen](http://i.imgur.com/t5ilLOn.png)

### Welcome screen

The welcome screen which the user is presented after the user logs into the system and rest services exposed via the Swagger UI 2.0.
Swagger helps with the generation of client code to invoke API's as well as documentation for rest API's.

![Welcome Screen](http://i.imgur.com/YUeP4GK.png)

### User REST service

User Resource management is categorized as follows GET, POST, DELETE and PUT
GET /user (would fetch all the users in the system)

This would list all the users in the system and display on the screen. There is pagination for user to navigate and 
obtain the subset of the results, including the search mechanisms.

#### User Invocation

![User Invokation](http://i.imgur.com/ujLsErC.png)

#### User listing

![User Listing](http://i.imgur.com/f1MYHtx.png)


## Technology stack used for this project

**JDK 8** ![JAVA](https://www.topjavablogs.com/spring/icon/favicon) : Many JDK 8 concepts being utilized for making pattern based development sweet, including Iterator Patterns, Singleton etc.

**EJB 3** ![EJB3](http://i.imgur.com/lSve51m.png): Personal choice of EJB3 rather than spring, however, EJB3 is lighter than Spring and comes out of the box in Wildfly.

**Swagger 2.0** ![SWAGGER](https://app.swaggerhub.com/img/favicon-32x32.png) : Rest API's can be easily invoked with the swagger UI and responses easily analyzed including the ability to provide documentation.

**Hibernate 3** ![HIBERNATE](http://i.imgur.com/iZpbzJu.png) : ORM framework that makes for using few HQL query. JPA with hibernate implementation.

**JPA 2.0** ![JPA](http://i.imgur.com/73KeY8Z.png) : The specification standard used for persistence.

** JAAS** ![JAAS](http://i.imgur.com/ge8PSaj.png) : The Java Authentication & Authorization Service. For user authentication and for the Login module interceptors.

**Junit 4/Open EJB** ![JUNIT](https://s3-ap-northeast-1.amazonaws.com/qiita-tag-image/bede974ba48a9bc1b123aac2ac22356ccf78da0e/medium.jpg?1450590252) : Testing with an actual embedded EJB container OpenEJB with in-memory DB as H2 database to promote an automated testing

**Python 2.7** ![PYTHON](http://i.imgur.com/oCzlCRF.jpg): Easier installation, Deployment and platform independent

**H2 Database** ![H2](http://i.imgur.com/d1opEOl.png) : Light DB which supports both in memory as well as file-based DB, Also comes out of the box with Wildfly/Jboss

**Wildfly** ![Wildfly](http://i.imgur.com/cig8k5v.gif) : Free open source, Lightweight and Production ready application server with the range of features.

**Maven 3** ![MAVEN](http://i.imgur.com/DnUDQZs.png) : Simple project management tool that enables easy dependency management

**Ant** ![ANT](http://i.imgur.com/FacmOB0.png) : Ant as a maven plugin for assembly and configuring and manipulating xml files (stanalone xml) including creation of installer files.

### Swagger2 UI

Swagger 2.0 is used to visualize API resources from its Swagger definition and generate, interactive documentation that 
can be hosted in any environment, allowing to easily get started with the API's.

`Swagger codegen` is used to auto-generate the models required to be used for the resources that are defined. This helps to keep it streamlined and in tandem both at the server as well as the client. There is also the provision to auto-generate the APIClient which could be used to invoke the apis without using the Swagger UI.

Following is the Swagger code generation task for auto-generating models in pom

```
<plugin>
    <groupId>io.swagger</groupId>
    <artifactId>swagger-codegen-maven-plugin</artifactId>
    <version>2.2.2</version>
    <executions>
        <execution>
            <goals>
                <goal>generate</goal>
            </goals>
            <configuration>
                <inputSpec>${basedir}/src/main/webapp/swagger.yaml</inputSpec>
                <language>java</language>
                <generateApis>false</generateApis>
                <generateModelTests>false</generateModelTests>
                <addCompileSourceRoot>false</addCompileSourceRoot>            
                <modelPackage>com.myproject.app.rest.generated.models</modelPackage>
                <configOptions>
                    <library>jersey2</library>
                    <dateLibrary>java8</dateLibrary>
                    <sourceFolder>/</sourceFolder>
                </configOptions>
                <environmentVariables>
                    <models></models>
                </environmentVariables>
            </configuration>
        </execution>
    </executions>
</plugin>
```

### JAAS

Java Authentication and authorization service is used to validate the users that are trying to access the resource. This is 
achieved by having 2 login modules 

* 1) Using a properties file users.properties and roles.properties added in JBoss server config directory
     The default username being set as super and password as super
     The login module flag is set as `sufficient` 
     
* 2) Using the Database login with the `usersQuery` containing the query to fetch the user password from the database.
     There are no users initially included in the database and hence, once the user logins in and adds a user with the REST API
     the same user can then log in with the credentials being passed.
     The login module flag is set as `required`
     
__Based on the flags, JAAS would attempt the following for authentication:__

    1) Check username/password against users.properties
    2) If auth succeeds, finish with successful authentication
    3) If auth fails, continue to the next module
    4) Check username/pass against the database `USER` table
    5) If auth succeeds, finish with successful authentication
    6) If auth fails, finish with failed authentication
     
```
<security-domain name="APP" cache-type="default">
    <authentication>
        <login-module code="RealmUsersRoles" flag="sufficient">
            <module-option name="usersProperties" value="${jboss.server.config.dir}/users.properties"/>
            <module-option name="rolesProperties" value="${jboss.server.config.dir}/roles.properties"/>
            <module-option name="password-stacking" value="useFirstPass"/>
            <module-option name="hashUserPassword" value="false"/>
        </login-module>
        <login-module code="org.jboss.security.auth.spi.DatabaseServerLoginModule" flag="required">
            <module-option name="dsJndiName" value="java:/myProjDatabase"/>
            <module-option name="principalsQuery" value="SELECT PASSWORD FROM USER WHERE LOGIN=?"/>
            <module-option name="hashAlgorithm" value="MD5"/>
            <module-option name="password-stacking" value="useFirstPass"/>
        </login-module>
    </authentication>
</security-domain>
```

## Running the tests

Tests are Run using JUnit4 with OpenEJB embedded container and H2 database. 
This gives a complete test environment from starting the Open EJB embedded server with H2 database for performing CRUD 
operations on the entities.

```
mvn clean install
```


### Breakdown into end to end tests

The tests created extend from an abstract class (AbstractDaoManager, AbstractJPARepository, AbstractRestManager) which under 
the hood loads some static data into the in-memory H2 database that is created and initialized before the tests start.

The static data is loaded using an XML with the use of DBUnit which is a JUnit extension to load data into the database.
This also enables to extend the static data that are loaded at the startup of tests. Hence, the JUnit tests can write which 
are light and address core aspects of what they are supposed to test without the additional noise.

Each module has a set of at least 10 tests covering all aspects of positive as well as negative test cases have been covered.

The Open EJB starts an embedded EJB container and the beans can be looked up using initial context and service methods invoked 

Following is a sample test to find a role record with name `Testing` 
```
public class RoleDaoTest extends AbstractDaoManager {
   
    public RoleDaoTest() throws NamingException {
        roleDao = (RoleDao) context.lookup("java:global/app-management/RoleDaoImpl");
    }
    
    @Test
    public void findExistingRole() {
        Optional<RoleEntity> role = roleDao.findByName("Testing");
        assertNotNull(role.get().getId());
    }
}
```

### And coding style tests

app-entities 
The tests within this module are focused on testing the entities especially creation, deletion, and updates of records in the database. They also focus on Generic constructs being used to make scalability of the application easy and hassle-free. 
With the use of generics, the core part of inserting entities remain loosely coupled and hence can accommodate extensibility.

The ejb-jar.xml enables initializing the BaseService class to be stateful EJB and with the persistence context injected into it. 
This is an example has to how we can use ejb-jar.xml to define lookup names and creating stateless or stateful session beans. 
Personally, I prefer annotations as it makes it easier to code rather than scrolling through huge XML files once a project becomes 
a huge application.

Following is one of the many tests used to find a record from the database with the Login name `Random`
```
    @Test
    public void testFindUser() throws Exception {
        List<UserEntity> users = baseService.findAll(UserEntity.class);
        assertTrue(users.stream()
                        .map(UserEntity::getLogin)
                        .filter("Random"::equals)
                        .findFirst()
                        .isPresent());
    }
```

app-management
This is the Dao layer of the application. Any Additional methods can be written in case there are specialized needs per resource which extend from Generic Persistence and Generic query handlers. The Dao implementation class by default have 
constructors that take in a search string which can be used as a default search parameter for the Resource(user/role) in general 

```
    public UserDaoImpl() {
        super(UserEntity.class, "login");
    }
```

The generic persistence handler has a Transactional attribute of mandatory for persist and update methods, and hence, 
in order to persist or update any entity in the database, the test methods need to be within a transactional caller without which the ORM would complain with EJBTransactionRequiredException. This was achieved by a wrapping the call within a new transaction with REQUIRES_NEW transactional attribute as seen in `AbstractDaoManager` class.

```
    @Test
    public void testDeleteUser() throws Exception {
        transactionalCaller.call(() -> userDao.delete(userDao.findByName("User").get()));
        assertFalse(userDao.findByName("User").isPresent());
    }
```

## Building With

* [Maven](https://maven.apache.org/) - Dependency Management
Maven 3.5 For building the project and dependency management. Also, configure Maven to use Java 8.
Once Maven is installed and setup navigate to the project directory to execute the following command

* mvn clean install

This will start building the project automatically running all the Tests and building and configuring the installer files with the 
usual following information

```
$ mvn clean install
[INFO] Scanning for projects...
[INFO] ------------------------------------------------------------------------
[INFO] Reactor Build Order:
[INFO]
[INFO] Maven Application
[INFO] Application Entities
[INFO] Application Management
[INFO] Rest Services Webapp
[INFO] Application Ear
[INFO] AngularJS Console
[INFO] Installer
```

### Installer ZIP files

The installer zip files are created under the project in the following directory structure
* app-installer\target\installer\zip\app-installer-0.0.1-SNAPSHOT.zip

This installer file can be directly taken and deployed in any environment with the steps mentioned in the previous section [Classic Installing](https://bitbucket.org/shourien/application/overview#markdown-header-getting-started-classic-installer)


## Versioning

Use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://bitbucket.org/shourien/application/branches). 

## Authors

* **Shourien** - *Initial work* - [Shourien](https://bitbucket.org/shourien)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Improvements

Liquibase:- for source control of Database (upgrades, creations etc) 
This is a great tool for a production database. It's better not to have to have the hbm2ddl auto set to updates done in this project but it would be simple to use liquibase to manage your database upgrades and installations.